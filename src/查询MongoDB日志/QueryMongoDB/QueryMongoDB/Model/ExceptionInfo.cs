﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace QueryMongoDB.Model
{
    [BsonIgnoreExtraElements]
    public class ExceptionInfo
    {
        public DateTime Time { get; set; }

        public Message Message { get; set; }
        //public ObjectId _id { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class Message
    {
        
        public string Value { get; set; }
    }
}
