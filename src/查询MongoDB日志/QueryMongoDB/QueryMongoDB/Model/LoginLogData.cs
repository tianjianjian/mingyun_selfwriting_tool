﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace QueryMongoDB.Model
{
    [BsonIgnoreExtraElements]
    public class LoginLogData
    {
        public  DateTime LogDate { get; set; }

        public string UserName { get; set; }
    }
}
