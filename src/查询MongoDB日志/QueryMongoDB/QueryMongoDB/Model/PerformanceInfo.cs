﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace QueryMongoDB.Model
{
    [BsonIgnoreExtraElements]
    public class PerformanceInfo
    {
        public DateTime Time { get; set; }

        public String ExecuteTime { get; set; }
    }
}
