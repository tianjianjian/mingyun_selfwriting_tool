﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QueryMongoDB.Model;

namespace QueryMongoDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txtSearch_Click(object sender, EventArgs e)
        {
            string ip = this.txtAdress.Text.Trim();
            string port = this.txtPort.Text.Trim();
            string dbName = this.txtDbName.Text.Trim();
            MongoDbHelper mh = new MongoDbHelper(ip, port, dbName);

            //登录
            List<LoginLogData> loginLog = mh.FindAll<LoginLogData>();
            LoginOutPut(loginLog);
            //异常
            var exception = mh.FindAll<ExceptionInfo>();
            ExceptionOutPut(exception);
            //性能
            var performance = mh.FindAll<PerformanceInfo>();
            PerOutPut(performance);
            //00:00:01.0727611
        }

        private void LoginOutPut(List<LoginLogData> loginLog)
        {
            this.txtLogInfo.Clear();
            var loginInfo = loginLog.GroupBy(t => t.LogDate.ToString("yyyy-MM")).ToList();
            foreach (var login in loginInfo)
            {
                this.txtLogInfo.AppendText(login.Key + ":" + login.Count() + "\r\n");
            }
        }

        private void ExceptionOutPut(List<ExceptionInfo> exception)
        {
            this.txtExInfo.Clear();
            var exceptionInfo = exception.GroupBy(t => t.Time.ToString("yyyy-MM")).ToList();
            foreach (var login in exceptionInfo)
            {
                this.txtExInfo.AppendText(login.Key + ":" + login.Count() + "\r\n");
            }
        }

        private void PerOutPut(List<PerformanceInfo> performance)
        {
            this.txtPerInfo.Clear();
            DateTime today = Convert.ToDateTime(DateTime.Now.ToShortDateString()).AddSeconds(10);
            //performance = performance.FindAll(t => t.ExecuteTime > "00:00:01.0727611");
            var performanceInfo = performance.Where(t => today < Convert.ToDateTime(t.ExecuteTime)).GroupBy(t => t.Time.ToString("yyyy-MM")).ToList();
            foreach (var login in performanceInfo)
            {
                this.txtPerInfo.AppendText(login.Key + ":" + login.Count() + "\r\n");
            }
        }
    }
}
