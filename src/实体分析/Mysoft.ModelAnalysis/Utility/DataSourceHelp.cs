﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace Mysoft.ModelAnalysis.Utility
{
    public static class DataSourceHelp
    {
        public static string GetEntityMetadataPath()
        {
            XmlDocument xml=new XmlDocument();
            xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DataSouce.xml"));
            var sitePath= xml.SelectSingleNode("/DataSouce/Path").InnerText;
            return sitePath + @"_metadata\Entity\";
        }
      
        public static List<string> GetModelDlls()
        {
            List<string> rst=new List<string>();
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DataSouce.xml"));
            var sitePath = xml.SelectSingleNode("/DataSouce/Path").InnerText;
            XmlNodeList nodeList=xml.SelectNodes("/DataSouce/ModelDlls/Item");
            foreach (XmlNode node in nodeList)
            {
                rst.Add(sitePath+@"bin\"+node.InnerText);
            }
            return rst;
        }
    }
}