﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Mysoft.ModelAnalysis.Models
{
    
    public class MetadataAttribute
    {

        /// <summary>
        /// 字段名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 字段类型
        /// </summary>
        public string AttributeType { get; set; }
    }
}