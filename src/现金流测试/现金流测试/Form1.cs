﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Mysoft.Slxt.FinanceMng.PzExport.WebService;

namespace 现金流测试
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            string url = this.txtUrl.Text;
            string content = this.txtContent.Text;
            if (string.IsNullOrEmpty(url))
            {
                MessageBox.Show("请输入WS地址");
                return;
            }

            if (string.IsNullOrEmpty(content))
            {
                MessageBox.Show("请输入传入内容");
                return;
            }
            StartTest();
        }


        private void StartTest()
        {
            this.btnTest.Enabled = false;

            try
            {
                //登录验证
                bool loginResult = EasLogin();
                if (loginResult == false)
                {
                    this.btnTest.Enabled = true;
                    return;
                }
                WSGLWebServiceFacadeSrvProxy wsglWebServiceFacadeSrvProxyService = new WSGLWebServiceFacadeSrvProxy()
                {
                    Url = $"{ConfigurationSettings.AppSettings["KingDeeWebService"]}/WSGLWebServiceFacade?wsdl"
                };
                List<WSWSVoucher> wswsVoucherList = XmlUtil.Deserialize(typeof(List<WSWSVoucher>), this.txtContent.Text) as List<WSWSVoucher>;

                //导入凭证
                string[] results = wsglWebServiceFacadeSrvProxyService.importVoucher(wswsVoucherList.ToArray(), 1, 0, 1);
                this.txtResult.Text = "";
                foreach (var result in results)
                {
                    this.txtResult.AppendText(result+"；");
                }
                this.btnTest.Enabled = true;
            }
            catch (Exception ex)
            {
                this.btnTest.Enabled = true;
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 金蝶登录验证
        /// </summary>
        /// <returns></returns>
        public virtual bool EasLogin()
        {
            string userName = ConfigurationSettings.AppSettings["KingDeeUserName"];
            string password = ConfigurationSettings.AppSettings["KingDeePassword"];
            string solution = ConfigurationSettings.AppSettings["KingDeeSolution"];
            string dataCentre = ConfigurationSettings.AppSettings["KingDeeDataCentre"];
            string language = ConfigurationSettings.AppSettings["KingDeeLanguage"];
            string databaseType = ConfigurationSettings.AppSettings["KingDeeDatabaseType"];
            string webService = ConfigurationSettings.AppSettings["KingDeeWebService"];
            try
            {
                EASLoginProxy easLoginProxyService = new EASLoginProxy { Url = $"{webService}/EASLogin" };
                WSContext loginContext = easLoginProxyService.login(userName, password, solution, dataCentre, language,
                    string.IsNullOrWhiteSpace(databaseType) ? 0 : int.Parse(databaseType));

                return string.IsNullOrWhiteSpace(loginContext.sessionId) == false;
            }
            catch/*为了给前端友好提示*/
            {
                MessageBox.Show("帐套的Web Service连接失败，请检查！");
                return false;
            }
        }


    }
}
