﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GenderModelCode.Business;

namespace GenderModelCode
{
    public partial class Form1 : Form
    {
        private string _url;
        private GenModelCode genModelCode = new GenModelCode();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            this._url = this.txtUrl.Text.Trim();
            if (string.IsNullOrEmpty(_url))
            {
                MessageBox.Show("请填写路径！");
            }

            genModelCode.Gen(this._url);
        }
    }
}
