﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GenderModelCode.Model
{
    [XmlRoot("MetadataEntity")]
    public class Entity
    {
        [XmlElement("Attributes")]
        public Attributes Attributes { get; set; }
    }


    public class Attributes
    {
        [XmlElement("MetadataAttribute")]
        public MetadataAttribute[] MetadataAttributeList { get; set; }
    }

    public class MetadataAttribute
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("DbType")]
        public string DbType { get; set; }

        [XmlAttribute("IsNullable")]
        public bool IsNullable { get; set; }

    }


    public static class DBType
    {
        public static readonly string Int = "int";
        public static readonly string Decimal = "decimal";
        public static readonly string Nvarchar = "nvarchar";
        public static readonly string Datetime = "datetime";
        public static readonly string Uniqueidentifier = "uniqueidentifier";
        public static readonly string Timestamp = "timestamp";//VersionNumber 
    }

}


