﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 检查财务接口重合表调整.Model
{
    public class Const
    {
        /// <summary>
        /// git地址
        /// </summary>
        public static readonly string GitUrl = System.Configuration.ConfigurationManager.AppSettings["gitUrl"];


        /// <summary>
        /// 原始tag
        /// </summary>
        public static readonly string 原始tag = System.Configuration.ConfigurationManager.AppSettings["原始tag"];

        /// <summary>
        /// 解析需要对比的表
        /// </summary>
        public static readonly string CompareTable = "AppData/Tables.txt";


    }
}
