﻿namespace 检查财务接口重合表调整
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNowFeature = new System.Windows.Forms.TextBox();
            this.txtRestult = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSqlBm = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "现有分支";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "表不一致结果";
            // 
            // txtNowFeature
            // 
            this.txtNowFeature.Location = new System.Drawing.Point(129, 8);
            this.txtNowFeature.Name = "txtNowFeature";
            this.txtNowFeature.Size = new System.Drawing.Size(466, 28);
            this.txtNowFeature.TabIndex = 2;
            this.txtNowFeature.Text = "feature/财务接口9月SP1";
            // 
            // txtRestult
            // 
            this.txtRestult.Location = new System.Drawing.Point(24, 101);
            this.txtRestult.Multiline = true;
            this.txtRestult.Name = "txtRestult";
            this.txtRestult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRestult.Size = new System.Drawing.Size(713, 303);
            this.txtRestult.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(634, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "检查";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 428);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "升级sql编码不为GB3213";
            // 
            // txtSqlBm
            // 
            this.txtSqlBm.Location = new System.Drawing.Point(24, 463);
            this.txtSqlBm.Multiline = true;
            this.txtSqlBm.Name = "txtSqlBm";
            this.txtSqlBm.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSqlBm.Size = new System.Drawing.Size(713, 273);
            this.txtSqlBm.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 748);
            this.Controls.Add(this.txtSqlBm);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtRestult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtNowFeature);
            this.Name = "Form1";
            this.Text = "财务接口检查";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNowFeature;
        private System.Windows.Forms.TextBox txtRestult;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSqlBm;
    }
}

