﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    public static class PathConst
    {
        /// <summary>
        /// 素有系统的的AppDll
        /// </summary>
        /// <remarks>
        /// 例如：
        /// <ModelDlls name="售楼系统" code="SL" ycurl="">
        /// <Item>Mysoft.Slxt.Common.dll</Item>
        /// <Item>Mysoft.Slxt.ProjectPrep.dll</Item>
        /// <Item>Mysoft.Slxt.PriceMng.dll</Item>
        /// <Item>Mysoft.Slxt.TradeMng.dll</Item>
        /// <Item>Mysoft.Slxt.FinanceMng.dll</Item>
        /// <Item>Mysoft.Slxt.SaleService.dll</Item>
        /// <Item>Mysoft.Slxt.OpportunityMng.dll</Item>
        /// <Item>Mysoft.Slxt.Scyx.dll</Item> 
        /// </ModelDlls>
        /// <ModelDlls name="售楼系统" code="SL" ycurl="">
        /// <Item>Mysoft.Slxt.Common.dll</Item>
        /// <Item>Mysoft.Slxt.ProjectPrep.dll</Item>
        /// <Item>Mysoft.Slxt.PriceMng.dll</Item>
        /// <Item>Mysoft.Slxt.TradeMng.dll</Item>
        /// <Item>Mysoft.Slxt.FinanceMng.dll</Item>
        /// <Item>Mysoft.Slxt.SaleService.dll</Item>
        /// <Item>Mysoft.Slxt.OpportunityMng.dll</Item>
        /// <Item>Mysoft.Slxt.Scyx.dll</Item> 
        /// </ModelDlls> 
        /// </remarks>
        public static readonly string AllAppDataSouce = @"App_Data\AllAppDataSouce.xml";
    }
}