﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDC与云测接口对接.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class AppDataSource
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 物理路径
        /// </summary>
        public string Path { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class AllAppDataSource
    {
        /// <summary>
        /// 模块dll
        /// </summary>
        public List<AppDataSource> AppDataSources { get; set; }

        /// <summary>
        /// 系统名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 系统code
        /// </summary>
        public string Code { get; set; }

    }
}
