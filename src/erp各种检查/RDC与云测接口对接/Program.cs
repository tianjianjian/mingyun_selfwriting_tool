﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using RDC与云测接口对接.Bussiness;
using Mysoft.Utility;

namespace RDC与云测接口对接
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var code = "";
                var dmAddress = "";
                var logAddress = "";
                if (args == null || args.Length == 0)
                {
                    throw new Exception("未传递系统code");
                }
                else
                {
                    code = args[0];
                    dmAddress = args[1];
                    if (args.Length >= 3)
                    {
                        logAddress = args[2];
                    }
                }
                //code = string.IsNullOrEmpty(code) ? "slxt" : code;
                //dmAddress = string.IsNullOrEmpty(dmAddress) ? @"D:\git\slxt3.0\src\00_根目录" : dmAddress;
                CalJson cal = new CalJson();
                var re = cal.GetYcForYcPt(code, dmAddress);
                FileHelper.WriteLog("结果", re, logAddress);
                //调用云测的接口给云测
                //string jsonStr = HttpHelper.Http("http://10.5.10.162:50780/test/GetInterfaceCoverage", "POST", "application/json;charset=utf-8", null, re);
                
            }
            catch (Exception ex)
            {
                FileHelper.WriteLog("错误日志" + Guid.NewGuid(), ex.ToString() + "\r\n入参：" + args.ToJson());
            }

        }
    }
}
