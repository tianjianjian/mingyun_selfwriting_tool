﻿using RDC与云测接口对接.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RDC与云测接口对接.Bussiness
{

    public static class DataSourceHelp
    {
        public static List<AllAppDataSource> GetAllModelDlls(string xmlPath, string dmAddress)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(xmlPath);
            XmlNodeList modelDlls = xml.SelectNodes("/DataSouce/ModelDlls");
            List<AllAppDataSource> allAppDataSources = new List<AllAppDataSource>();
            foreach (XmlNode node in modelDlls)
            {
                AllAppDataSource allAppDataSource = new AllAppDataSource();
                allAppDataSource.Name = node.Attributes["name"].Value.ToString();
                allAppDataSource.Code = node.Attributes["code"].Value.ToString(); 

                var itemNodes = node.SelectNodes("Item");
                List<AppDataSource> appDataSources = new List<AppDataSource>();
                foreach (XmlNode itemNode in itemNodes)
                {
                    var name = itemNode.Attributes["name"].Value.ToString();
                    appDataSources.Add(new AppDataSource
                    {
                        Name = name,
                        Path = Path.Combine(dmAddress, @"bin\" + itemNode.InnerText)
                    });
                }
                allAppDataSource.AppDataSources = appDataSources;
                allAppDataSources.Add(allAppDataSource);
            }
            return allAppDataSources;
        }
    }
}
