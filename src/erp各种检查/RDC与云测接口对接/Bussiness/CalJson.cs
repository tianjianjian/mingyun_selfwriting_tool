﻿using Mysoft.Utility;
using RDC与云测接口对接.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using 检查页面开放.Models;

namespace RDC与云测接口对接.Bussiness
{
    public class CalJson
    {
        YunCeForSjzx yunCeForSjzx = new YunCeForSjzx();
        private readonly GetAppApi _getAppApi = new GetAppApi();
        public string GetYcForYcPt(string modelCode, string dmAddress)
        {
            if (string.Equals(modelCode, "SJZX", StringComparison.OrdinalIgnoreCase))
            {
                return yunCeForSjzx.Search();
            }
            YcResult re = GetYcRe(modelCode,dmAddress);
            re.Models = null;
            return re.ToJson();
        }

        private YcResult GetYcRe(string modelCode,string dmAddress)
        {
            var pzAddress = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathConst.AllAppDataSouce);
            List<AllAppDataSource> allAppDataSources = DataSourceHelp.GetAllModelDlls(pzAddress, dmAddress);
            var app = allAppDataSources.FirstOrDefault(t => string.Equals(t.Code, modelCode, StringComparison.OrdinalIgnoreCase));
            if (app == null)
            {
                throw new Exception("请配置系统");
            }
            List<Model> models = _getAppApi.GetModels(app.AppDataSources);
            List<MethodInfo> allMethod = new List<MethodInfo>();
            foreach (var model in models)
            {
                allMethod.AddRange(model.MethodList);
            }
            List<InterFaceResult> interFaceResults = new List<InterFaceResult>();
            var zt = new InterFaceResult
            {
                Remark = "整体",
                Count = allMethod.Count,
                YcCount = allMethod.Count(t => t.YCIsCover == true),
                MethodList = allMethod
            };
            zt.Fgl = zt.YcCount * 1.00 / zt.Count * 100;
            interFaceResults.Add(zt);

            var dsf = new InterFaceResult
            {
                Remark = "外部第三方调用接口",
                Count = allMethod.Count(t => t.IsOpen == true),
                YcCount = allMethod.Count(t => t.IsOpen == true && t.YCIsCover == true),
                MethodList = allMethod.FindAll(t => t.IsOpen == true)
            };
            dsf.Fgl = dsf.YcCount * 1.00 / dsf.Count * 100;
            interFaceResults.Add(dsf);

            var kzxt = new InterFaceResult
            {
                Remark = "跨子系统及业务单元调用接口",
                Count = allMethod.Count(t => t.IsOpen.GetValueOrDefault() == false && t.IsPublic == true),
                YcCount = allMethod.Count(t => t.IsOpen.GetValueOrDefault() == false && t.IsPublic == true && t.YCIsCover == true),
                MethodList = allMethod.FindAll(t => t.IsOpen.GetValueOrDefault() == false && t.IsPublic == true)
            };
            kzxt.Fgl = kzxt.YcCount * 1.00 / kzxt.Count * 100;
            interFaceResults.Add(kzxt);

            var nbjk = new InterFaceResult
            {
                Remark = "业务单元内部接口",
                Count = allMethod.Count(t => t.IsPublic.GetValueOrDefault() == false),
                YcCount = allMethod.Count(t => t.IsPublic.GetValueOrDefault() == false && t.YCIsCover == true),
                MethodList = allMethod.FindAll(t => t.IsPublic.GetValueOrDefault() == false)
            };
            nbjk.Fgl = nbjk.YcCount * 1.00 / nbjk.Count * 100;
            interFaceResults.Add(nbjk);

            YcResult ycResult = new YcResult();
            ycResult.InterFaceResults = interFaceResults;
            ycResult.Models = models;
            return ycResult;
        }
    }
}
