﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MdDocGenerator.Util;

namespace MdDocGenerator
{
    static class Program
    {
        static void Main(string[] args)
        {
            var outputPath = ConfigHelper.OutputPath;
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            outputPath = new DirectoryInfo(outputPath).FullName;
            var files = Directory.GetFiles(ConfigHelper.SourcePath, ConfigHelper.Dll, SearchOption.AllDirectories)
                .ToList();
            Console.WriteLine("开始生成Markdown文档...");
            new MarkdownGenerator().Generate(files, outputPath);
            Console.WriteLine("Markdown文档生成完成");
            Console.ReadKey();
        }

    }

    public class MarkdownType
    {
        private static Dictionary<Type, object> _valueProvider = new Dictionary<Type, object>()
        {
            {typeof(string),"abc"},
            {typeof(DateTime),DateTime.Now.ToString("yyyy-MM-dd")},
            {typeof(decimal),123.12M},
            {typeof(Guid),$"{Guid.NewGuid().ToString().ToUpper()}"},
            {typeof(bool),false },
            {typeof(char),"1" },
            {typeof(byte[]),null },
            {typeof(Enum),1 },
            {typeof(object),null },
            {typeof(double),100.00D },
            {typeof(int),null},
        };
        public Type Type { get; set; }

        public object Vaue { get; set; }

        public Type Owner { get; set; }

        public void SetValue(Type type)
        {
            this.Type = type;
            if (_valueProvider.TryGetValue(type, out object foo))
            {
                this.Vaue = foo;
            }
        }
    }

    static class ValueBuilder
    {

        private static Dictionary<Type, object> _valueProvider = new Dictionary<Type, object>()
        {
            {typeof(string),"abc"},
            {typeof(DateTime),DateTime.Now.ToString("yyyy-MM-dd")},
            {typeof(decimal),123.12M},
            {typeof(Guid),$"{Guid.NewGuid().ToString().ToUpper()}"},
            {typeof(bool),false },
            {typeof(char),"1" },
            {typeof(byte[]),null },
            {typeof(Enum),1 },
            {typeof(object),null },
            {typeof(double),100.00D },
            {typeof(int),null},
        };

        public static void SetValue(object inputObject, string propertyName, object propertyVal)
        {
            //find out the type
            Type type = inputObject.GetType();

            //get the property information based on the type
            System.Reflection.PropertyInfo propertyInfo = type.GetProperty(propertyName);

            //find the property type
            Type propertyType = propertyInfo.PropertyType;

            //Convert.ChangeType does not handle conversion to nullable types
            //if the property type is nullable, we need to get the underlying type of the property
            var targetType = IsNullableType(propertyInfo.PropertyType) ? Nullable.GetUnderlyingType(propertyInfo.PropertyType) : propertyInfo.PropertyType;

            //Returns an System.Object with the specified System.Type and whose value is
            //equivalent to the specified object.
            propertyVal = Convert.ChangeType(propertyVal, targetType);

            //Set the value of the property
            propertyInfo.SetValue(inputObject, propertyVal, null);

        }

        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
    }

}