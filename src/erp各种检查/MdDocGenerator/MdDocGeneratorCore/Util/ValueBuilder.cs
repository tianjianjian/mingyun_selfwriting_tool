﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Mysoft.Map6.Core.EntityBase;

namespace MdDocGenerator.Util
{
    public class ValueBuilder
    {
        private static readonly Dictionary<Type, Func<object>> ValueProvider = new Dictionary<Type, Func<object>>()
        {
            {typeof(string),()=>"abc"},
            {typeof(DateTime),()=>DateTime.Now.ToString("yyyy-MM-dd")},
            {typeof(decimal),()=>123.12M},
            {typeof(Guid),()=>$"{Guid.NewGuid().ToString().ToUpper()}"},
            {typeof(bool),()=>true },
            {typeof(char),()=>"1" },
            {typeof(Enum),()=>1 },
            {typeof(double),()=>123.00D },
            {typeof(int),()=>1}
        };
        //public object Build(Type t)
        //{
        //    if (ValueProvider.TryGetValue(t, out Func<object> val))
        //    {
        //        return val.Invoke();
        //    }

        //    if (t.IsGenericType)
        //    {
        //        var singleType = t.GenericTypeArguments.First();
        //        return Build(singleType);
        //    }
        //}
    }
}