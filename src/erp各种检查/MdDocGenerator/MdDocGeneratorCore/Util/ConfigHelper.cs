﻿using System.Configuration;

namespace MdDocGenerator.Util
{
    public class ConfigHelper
    {
        public static string TemplatePath => GetConfigValue("TemplatePath");

        public static string OutputPath => GetConfigValue("OutputPath");

        public static string SourcePath => GetConfigValue("SourcePath");

        public static string Dll => GetConfigValue("Dll");

        public static string IgnoreMethods => GetConfigValue("IgnoreMethods");

        public static string IgnoreServices => GetConfigValue("IgnoreServices");

        public static string IgnoreProperty => GetConfigValue("IgnoreProperty");

        public static string OutputModule => GetConfigValue("OutputModule");

        private static string GetConfigValue(string key)
        {
           return ConfigurationManager.AppSettings[key]??"";
        }
    }
}