﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MdDocGenerator.Client
{
    /// <summary>
    /// 提供给第三方端调用
    /// </summary>
    public class MdDocClient
    {
        public List<Client.Model.MyMethodInfo> QueryMethodRemark(List<string> dlls)
        {
            return new MarkdownGenerator().QueryMethodRemark(dlls);
        }
    }
}
