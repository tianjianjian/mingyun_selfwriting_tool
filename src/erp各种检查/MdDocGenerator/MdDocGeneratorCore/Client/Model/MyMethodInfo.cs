﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MdDocGenerator.Client.Model
{
    public class MyMethodInfo
    {
        /// <summary>
        /// 方法全称
        /// </summary>
        public string MethodFullName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
