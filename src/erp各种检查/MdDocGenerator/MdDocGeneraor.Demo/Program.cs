﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MdDocGeneraor.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stu = new Student();
            DumpObjectTree(stu);
            Console.ReadKey();
        }
        static void DumpObjectTree(object propValue, int level = 1)
        {
            if (propValue == null)
                return;

            var childProps = propValue.GetType().GetProperties();
            foreach (var prop in childProps)
            {
                var name = prop.Name;
                var value = prop.GetValue(propValue, null);

                // add some left padding to make it look like a tree
                Console.WriteLine("".PadLeft(level * 4, ' ') + "{0}={1}", name, value);

                // call again for the child property
                DumpObjectTree(value, level + 1);
            }
        }
    }

    public class Student
    {
        public string Name { get; set; }

        public string Sex { get; set; }

        public int Age { get; set; }

        public List<Address> Addresses { get; set; }

        public List<Course> Courses { get; set; }

        public School School { get; set; }
    }

    public class School
    {
        public string Name { get; set; }

        public Address Address { get; set; }
    }

    public class Address
    {
        public string Name { get; set; }
    }

    public class Course
    {
        public string Name { get; set; }

        public int Time { get; set; }
    }
}
