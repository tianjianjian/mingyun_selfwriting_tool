﻿using EasyUtility.Excel;
using Mysoft.Upgrade.HelpTool.Business;
using Mysoft.Upgrade.HelpTool.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mysoft.Upgrade.HelpTool
{
    public partial class Form1 : Form
    {
        private readonly VersionBusiness VersionBusiness = new VersionBusiness();
        public Form1()
        {
            InitializeComponent();
            InitData();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog P_File_Folder = new FolderBrowserDialog();

            if (P_File_Folder.ShowDialog() == DialogResult.OK)
            {
                this.tbDmUrl.Text = P_File_Folder.SelectedPath;
            }
        }

        private void TbDmUrl_TextChanged(object sender, EventArgs e)
        {

        }

        private void InitData()
        {
            this.cbOldVersion.DataSource = VersionBusiness.GetOldList();
            this.cbNowVersion.DataSource = VersionBusiness.GetNowList();
        }


        private string _oldVersion;
        private string _nowVersion;
        private string _dmUrl;
        private string _customizePath;
        private string _excelName;
        private string _excelPath;

        private void Button2_Click(object sender, EventArgs e)
        {
            _oldVersion = this.cbOldVersion.Text.Trim();
            _nowVersion = this.cbNowVersion.Text.Trim();
            _dmUrl = this.tbDmUrl.Text.Trim();
            _customizePath =_dmUrl;

            //校验
            if (ReplaceValidate() == false)
            {
                return;
            }

            ReplaceEnumBusiness replaceEnumBusiness = new ReplaceEnumBusiness(GetEnumReplaceDetail(), _customizePath);
            replaceEnumBusiness.StartReplace();
        }

        /// <summary>
        /// 替换前校验
        /// </summary>
        /// <returns></returns>
        private bool ReplaceValidate()
        {
            if (string.IsNullOrEmpty(_oldVersion) == true)
            {
                MessageBox.Show("请选择" + this.lblOldVersion.Text);
                return false;
            }

            if (string.IsNullOrEmpty(_nowVersion) == true)
            {
                MessageBox.Show("请选择" + this.lblNowVersion.Text);
                return false;
            }

            if (string.IsNullOrEmpty(_dmUrl) == true)
            {
                MessageBox.Show("请输入" + this.lblDmUrl.Text);
                return false;
            }
            else
            {
                //判断文件是否存在
                if (Directory.Exists(_customizePath) == false)
                {
                    MessageBox.Show("代码地址中不存在二开元数据！");
                    return false;
                }
            }



            _excelName = $"{_oldVersion}-{_nowVersion}_枚举替换.xlsx";
            _excelPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"AppData\\{_excelName}");
            //判断文件是否存在
            if (File.Exists(_excelPath) == false)
            {
                MessageBox.Show("没有匹配的更新版本，请联系管理员！");
                return false;
            }

            return true;
        }

        private List<EnumReplaceDetail> GetEnumReplaceDetail()
        {
            List<EnumReplaceDetail> enumReplaceDetails;
            ExcelHelper excelHelper = new ExcelHelper(FilePathConst.EnumReplaceDetail);
            using (FileStream fs = new FileStream(_excelPath, FileMode.Open, FileAccess.Read))
            {
                enumReplaceDetails = excelHelper.ExcelToAllList<EnumReplaceDetail>(fs, _excelName);
            }
            return enumReplaceDetails;
        }
    }
}
