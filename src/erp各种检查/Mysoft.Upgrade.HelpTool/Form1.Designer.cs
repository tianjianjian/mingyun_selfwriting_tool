﻿namespace Mysoft.Upgrade.HelpTool
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOldVersion = new System.Windows.Forms.Label();
            this.lblNowVersion = new System.Windows.Forms.Label();
            this.lblDmUrl = new System.Windows.Forms.Label();
            this.cbOldVersion = new System.Windows.Forms.ComboBox();
            this.cbNowVersion = new System.Windows.Forms.ComboBox();
            this.tbDmUrl = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOldVersion
            // 
            this.lblOldVersion.AutoSize = true;
            this.lblOldVersion.Location = new System.Drawing.Point(38, 42);
            this.lblOldVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOldVersion.Name = "lblOldVersion";
            this.lblOldVersion.Size = new System.Drawing.Size(134, 18);
            this.lblOldVersion.TabIndex = 0;
            this.lblOldVersion.Text = "产品原版本号：";
            // 
            // lblNowVersion
            // 
            this.lblNowVersion.AutoSize = true;
            this.lblNowVersion.Location = new System.Drawing.Point(40, 96);
            this.lblNowVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNowVersion.Name = "lblNowVersion";
            this.lblNowVersion.Size = new System.Drawing.Size(134, 18);
            this.lblNowVersion.TabIndex = 1;
            this.lblNowVersion.Text = "产品现版本号：";
            // 
            // lblDmUrl
            // 
            this.lblDmUrl.AutoSize = true;
            this.lblDmUrl.Location = new System.Drawing.Point(74, 158);
            this.lblDmUrl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDmUrl.Name = "lblDmUrl";
            this.lblDmUrl.Size = new System.Drawing.Size(98, 18);
            this.lblDmUrl.TabIndex = 2;
            this.lblDmUrl.Text = "代码地址：";
            // 
            // cbOldVersion
            // 
            this.cbOldVersion.FormattingEnabled = true;
            this.cbOldVersion.Location = new System.Drawing.Point(230, 42);
            this.cbOldVersion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbOldVersion.Name = "cbOldVersion";
            this.cbOldVersion.Size = new System.Drawing.Size(348, 26);
            this.cbOldVersion.TabIndex = 3;
            // 
            // cbNowVersion
            // 
            this.cbNowVersion.FormattingEnabled = true;
            this.cbNowVersion.Location = new System.Drawing.Point(230, 96);
            this.cbNowVersion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbNowVersion.Name = "cbNowVersion";
            this.cbNowVersion.Size = new System.Drawing.Size(348, 26);
            this.cbNowVersion.TabIndex = 4;
            // 
            // tbDmUrl
            // 
            this.tbDmUrl.AccessibleDescription = "";
            this.tbDmUrl.AccessibleName = "";
            this.tbDmUrl.Location = new System.Drawing.Point(230, 158);
            this.tbDmUrl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbDmUrl.Name = "tbDmUrl";
            this.tbDmUrl.Size = new System.Drawing.Size(834, 28);
            this.tbDmUrl.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1074, 154);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 34);
            this.button1.TabIndex = 6;
            this.button1.Text = "浏览";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label4.Location = new System.Drawing.Point(230, 200);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(485, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "地址示例：C:\\公司\\git\\slxt2.0\\src\\00_根目录\\_metadata";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(440, 320);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 34);
            this.button2.TabIndex = 8;
            this.button2.Text = "替换";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 372);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbDmUrl);
            this.Controls.Add(this.cbNowVersion);
            this.Controls.Add(this.cbOldVersion);
            this.Controls.Add(this.lblDmUrl);
            this.Controls.Add(this.lblNowVersion);
            this.Controls.Add(this.lblOldVersion);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "枚举替换";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOldVersion;
        private System.Windows.Forms.Label lblNowVersion;
        private System.Windows.Forms.Label lblDmUrl;
        private System.Windows.Forms.ComboBox cbOldVersion;
        private System.Windows.Forms.ComboBox cbNowVersion;
        private System.Windows.Forms.TextBox tbDmUrl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
    }
}

