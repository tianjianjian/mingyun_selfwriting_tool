﻿using Mysoft.Upgrade.HelpTool.Model;
using Mysoft.Upgrade.HelpTool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mysoft.Upgrade.HelpTool.Utility;

namespace Mysoft.Upgrade.HelpTool.Business
{
    public class VersionBusiness
    {
        private Model.Version Version
        {
            get; set;
        }

        public VersionBusiness()
        {
            this.Version = XMlHelper.XmlToObj<Model.Version>(FileHelper.GetText(FilePathConst.VersionXml));
        }

        /// <summary>
        /// 获取老的版本号
        /// </summary>
        /// <returns></returns>
        public List<string> GetOldList()
        {
            var oldItemList = Version.ItemList.DeepCopy();
            if (oldItemList.Count > 0)
            {
                oldItemList.RemoveAt(oldItemList.Count - 1);
            }
            return oldItemList;
        }

        /// <summary>
        /// 获取现在的版本号
        /// </summary>
        /// <returns></returns>
        public List<string> GetNowList()
        {
            var newItemList = Version.ItemList.DeepCopy();
            if (newItemList.Count > 0)
            {
                newItemList.RemoveAt(0);
            }
            return newItemList;
        }
    }
}
