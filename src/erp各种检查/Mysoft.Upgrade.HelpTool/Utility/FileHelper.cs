﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Upgrade.HelpTool.Utility
{
    public class FileHelper
    {
        /// <summary>
        /// 获取文本
        /// </summary>
        /// <param name="path"></param>
        public static string GetText(string path)
        {
            return System.IO.File.ReadAllText(path);
        }
        /// <summary>
        /// byte[] 转stream
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static Stream bytetTostream(byte[] buffer)
        {
            Stream stream = new MemoryStream(buffer);
            stream.Seek(0, SeekOrigin.Begin);
            //设置stream的position为流的开始
            return stream;
        }
    }
}