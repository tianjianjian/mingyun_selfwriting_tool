﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Mysoft.Upgrade.HelpTool.Utility
{
    public class XMlHelper
    {
        public static T XmlToObj<T>(string xml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xml);
            T t = (T)xs.Deserialize(reader);
            return t;
        }
    }

}
