﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Mysoft.Upgrade.HelpTool.Model
{
    [XmlRoot(ElementName="version")]
    public class Version
    {
        [XmlElement(ElementName= "item")]
        public List<string> ItemList { get; set; }
    }
    
}
