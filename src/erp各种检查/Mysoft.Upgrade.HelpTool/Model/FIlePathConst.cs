﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Upgrade.HelpTool.Model
{
    public class FilePathConst
    {
        /// <summary>
        /// 版本号清单地址
        /// </summary>
        public static readonly string VersionXml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppData\\Version.xml");

        /// <summary>
        /// 枚举清单模板
        /// </summary>
        public static readonly string EnumReplaceDetail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppData\\EnumReplaceDetail.xml");
    }
}
