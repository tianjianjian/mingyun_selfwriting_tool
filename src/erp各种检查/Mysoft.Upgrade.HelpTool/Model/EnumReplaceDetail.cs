﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Upgrade.HelpTool.Model
{
    public class EnumReplaceDetail
    {
        public string OldEnum { get; set; }

        public string NowEnum { get; set; }
    }
}
