﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Utility
{
    public enum FeatureTypeEnum
    {
        Feature = 1,

        Tag = 2
    }

    public class GitHelper
    {
        private readonly string path;
        public GitHelper()
        {
            path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Git\" + Guid.NewGuid().ToString());
            PathHelper.CreatePath(path);
        }

        public void Clone(string gitPath, string feature, FeatureTypeEnum featureTypeEnum)
        {
            string cloneFeature = "";
            if (string.IsNullOrEmpty(feature) == false && featureTypeEnum == FeatureTypeEnum.Feature)
            {
                cloneFeature = "-b " + feature + " ";
            }
            string cloneCmd = "git clone " + cloneFeature + gitPath;

            //进入文件&clone
            RumCmdHelper.RumCmd("cd /d " + path + "&" + cloneCmd);

            if (string.IsNullOrEmpty(feature) == false && featureTypeEnum == FeatureTypeEnum.Tag)
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                RumCmdHelper.RumCmd("cd /d " + directoryInfo.GetDirectories().First().FullName + "&" + "git checkout " + feature);
            }
        }

        public string GetRootPath()
        {
            return path;
        }
        public string GetSrcPath()
        {
            //slxt_cwjk\src
            DirectoryInfo dicDirectoryInfo = new DirectoryInfo(path);
            return Path.Combine(dicDirectoryInfo.GetDirectories().First().FullName, "src");
        }

         

    }
}
