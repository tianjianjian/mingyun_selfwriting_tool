﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Utility
{
    public class DataAccess
    {
        private readonly SqlConnection Connection;
        public DataAccess(string connStr)
        {
            Connection = new SqlConnection(connStr);
        }

        public DataSet ExecuteDataset(string commandText, CommandType commandType, SqlParameter[] paras)
        {
            return this.PExecuteDataset(commandText, commandType, 0, paras);
        }
        public DataSet ExecuteDataset(string commandText, CommandType commandType, int timeOut, SqlParameter[] paras)
        {
            return this.PExecuteDataset(commandText, commandType, timeOut, paras);
        }
        private DataSet PExecuteDataset(string commandText, CommandType commandType, int timeOut, SqlParameter[] commandParameters)
        {
            var set2 = new DataSet();
            try
            {
                var connection = Connection;
                if (connection == null)
                {
                    throw new ArgumentNullException("connection");
                }
                var da = new SqlDataAdapter(commandText, connection);
                da.SelectCommand.CommandType = commandType;
                if (timeOut > 0)
                {
                    da.SelectCommand.CommandTimeout = timeOut;
                }
                if (commandParameters != null && commandParameters.Length != 0)
                {
                    da.SelectCommand.Parameters.AddRange(commandParameters);
                }
                da.Fill(set2);

            }
            catch (Exception exception)
            {
                throw new Exception("数据访问出错！", exception);
            }
            return set2;
        }


        public int ExecuteNonQuery(string commandText, SqlParameter[] commandParameters)
        {
            return this.ExecuteNonQuery(commandText, CommandType.Text, 0, commandParameters);
        }
        public int ExecuteNonQuery(string commandText, CommandType commandType, SqlParameter[] commandParameters)
        {
            return this.ExecuteNonQuery(commandText, commandType, 0, commandParameters);
        }
        public int ExecuteNonQuery(string commandText, CommandType commandType, int timeOut, SqlParameter[] commandParameters)
        {
            return this.PExecuteNonQuery(commandText, commandType, timeOut, commandParameters);
        }
        private int PExecuteNonQuery(string commandText, CommandType commandType, int timeout, SqlParameter[] commandParameters)
        {
            int num2;
            var connection = Connection;
            try
            {
                if (connection == null)
                {
                    throw new ArgumentNullException("connection");
                }
                var cmd = new SqlCommand(commandText, connection);
                cmd.CommandType = commandType;
                if (timeout > 0)
                    cmd.CommandTimeout = timeout;
                if (commandParameters != null)
                {
                    cmd.Parameters.AddRange(commandParameters);
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                int num = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                num2 = num;
            }
            catch (Exception exception)
            {
                throw new Exception("数据访问出错！", exception);
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                    connection.Close();
            }
            return num2;
        }


        public object ExecuteScalar(string commandText, CommandType commandType, SqlParameter[] commandParas)
        {
            return this.ExecuteScalar(commandText, commandType, 0, commandParas);
        }
        public object ExecuteScalar(string commandText, CommandType commandType, int timeOut, SqlParameter[] commandParas)
        {
            return this.PExecuteScalar(commandText, commandType, timeOut, commandParas);
        }
        private object PExecuteScalar(string commandText, CommandType commandType, int timeOut, DbParameter[] commandParameters)
        {
            object obj3;
            var connection = Connection;
            try
            {
                if (connection == null)
                {
                    throw new ArgumentNullException("connection");
                }
                var cmd = new SqlCommand(commandText, connection);
                cmd.CommandType = commandType;
                if (timeOut > 0)
                    cmd.CommandTimeout = timeOut;
                if (commandParameters != null)
                {
                    cmd.Parameters.AddRange(commandParameters);
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                object obj2 = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                obj3 = obj2;
            }
            catch (Exception exception)
            {
                throw new Exception("数据访问出错！", exception);
            }
            finally
            {
                if (connection != null && connection.State == ConnectionState.Open)
                    connection.Close();
            }
            return obj3;
        }

        public IDataReader ExecuteDataReader(string sql)
        {
            return ExecuteDataReader(sql, null);
        }
        public IDataReader ExecuteDataReader(string sql, SqlParameter[] commandParas)
        {
            return ExecuteDataReader(sql, CommandType.Text, commandParas);
        }
        public IDataReader ExecuteDataReader(string sql, CommandType commandType, SqlParameter[] commandParas)
        {
            return PExecuteDataReader(sql, commandType, 0, commandParas);
        }
        private SqlDataReader PExecuteDataReader(string commandText, CommandType commandType, int timeOut, DbParameter[] commandParameters)
        {
            SqlDataReader obj3;
            var connection = Connection;
            try
            {
                if (connection == null)
                {
                    throw new ArgumentNullException("connection");
                }
                var cmd = new SqlCommand(commandText, connection);
                cmd.CommandType = commandType;
                if (timeOut > 0)
                    cmd.CommandTimeout = timeOut;
                if (commandParameters != null)
                {
                    cmd.Parameters.AddRange(commandParameters);
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                var obj2 = cmd.ExecuteReader();
                cmd.Parameters.Clear();
                obj3 = obj2;
            }
            catch (Exception exception)
            {
                throw new Exception("数据访问出错！", exception);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
            return obj3;
        }
    }
}
