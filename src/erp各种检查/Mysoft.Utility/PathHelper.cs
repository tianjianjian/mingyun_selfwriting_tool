﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Utility
{
    public class PathHelper
    {
        public static void CreatePath(string path)
        {
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void DeletePath(string path)
        {
            try
            {
                if (Directory.Exists(path) == true)
                {

                    Directory.Delete(path, true);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
