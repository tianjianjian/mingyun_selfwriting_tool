﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Utility.InstanFactory
{
    public class Factory
    {
        private static Dictionary<string, IService> _dic = new Dictionary<string, IService>();

        public static void Register<T>(string code) where T : IService, new()
        {
            if (_dic.ContainsKey(code) == false)
            {
                _dic.Add(code, new T());
            }
        }


        public static IService GetInstance(string code)
        {
            if (_dic.ContainsKey(code) == false)
            {
                throw new Exception("实现类没有注册！");
            }

            _dic.TryGetValue(code, out IService serviceInstance);
            return serviceInstance;
        }
    }
}
