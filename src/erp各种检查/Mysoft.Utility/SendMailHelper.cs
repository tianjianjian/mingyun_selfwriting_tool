﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Mysoft.Utility
{

    public class SendMailHelper
    {
        public static void Send(SendMainDto sendMainDto)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(sendMainDto.SendEmail);//
            foreach (var toEmain in sendMainDto.ToEmails)
            {
                mail.To.Add(toEmain);
            }
            mail.Subject = sendMainDto.Subject;
            mail.Body = sendMainDto.Body;
            mail.IsBodyHtml = sendMainDto.IsBodyHtml;//mail body是否为html 

            using (SmtpClient SmtpServer = new SmtpClient(sendMainDto.SMTP))//
            {
                SmtpServer.Port = sendMainDto.Port;//
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.Credentials = new System.Net.NetworkCredential(sendMainDto.SendEmail, sendMainDto.SendEmailPwd);
                SmtpServer.EnableSsl = sendMainDto.EnableSsl;//
                SmtpServer.Send(mail);
            }
        }
    }


    public class SendMainDto
    {
        /// <summary>
        /// 发送人邮箱
        /// </summary>
        public string SendEmail { get; set; }

        /// <summary>
        /// 发送人密码
        /// </summary>
        public string SendEmailPwd { get; set; }

        /// <summary>
        /// 接送人邮箱
        /// </summary>
        public List<string> ToEmails { get; set; }

        /// <summary>
        /// 主题
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 内容是否是html
        /// </summary>
        public bool IsBodyHtml { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// SMTP
        /// </summary>
        public string SMTP { get; set; }

        /// <summary>
        /// EnableSsl
        /// </summary>
        public bool EnableSsl { get; set; }
    }
}
