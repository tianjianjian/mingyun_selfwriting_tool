﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using _3._0检查.Model;
using Mysoft.Utility;

namespace _3._0检查.Business.检查domain的非internal方法
{
    /// <summary>
    /// 获取功能包中的多语言、没有写的多语言
    /// </summary>
    public class GetGnbLang
    {
        /// <summary>
        /// 功能包使用到的多语言
        /// </summary>
        private List<string> usedLangs = new List<string>();
        /// <summary>
        /// 功能包中已经定义的多语言
        /// </summary>
        private List<StringKey> alreadyLangs = new List<StringKey>();

        /// <summary>
        /// 根目录地址
        /// </summary>
        private string SrcPath = "";

        /// <summary>
        /// 入口：获取未定义的多语言
        /// </summary>
        /// <returns></returns>
        public List<string> GetLangList(string path)
        {
            this.SrcPath = path;
            GetLange(path);
            usedLangs = usedLangs.Distinct().ToList();

            return GetNoDefineKey();
        }

        /// <summary>
        /// 获取已有的多语言
        /// </summary>
        /// <param name="fileInfo"></param>
        private void GetAlreadyLang(FileInfo fileInfo)
        {
            var text = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);
            LangModel functionModel = DeserializeToObject<LangModel>(text);
            if (functionModel.Language == "zh-CHS")
            {
                alreadyLangs = functionModel.List.StringKeyList.ToList();
            }
        }
        /// <summary>
        /// 获取功能包中未定义的key
        /// </summary>
        /// <returns></returns>
        private List<string> GetNoDefineKey()
        {
            usedLangs = usedLangs.Distinct().ToList();
            List<string> noDeFines = new List<string>();
            foreach (var used in usedLangs)
            {
                var alreadyLang = alreadyLangs.FirstOrDefault(t => t.Key == used);
                if (alreadyLang == null)
                {
                    noDeFines.Add(used);
                }
            }
            return noDeFines;
        }

        private void GetLange(string path)
        {
            var dir = new DirectoryInfo(path);
            ILangeSearch search = null;
            foreach (var fileInfo in dir.GetFiles())
            {
                //多语言文件不用处理
                if (fileInfo.Directory.FullName.EndsWith(@"_metadata\Langs") == true)
                {
                    GetAlreadyLang(fileInfo);
                    continue;
                }
                switch (fileInfo.Extension)
                {
                    case ".js":
                        //如果是多语言设置
                        if (fileInfo.Name.EndsWith("LangRes.js"))
                        {
                            usedLangs.AddRange(LangJsDo(fileInfo));
                        }
                        else
                        {
                            search = new JsSearch();
                        }
                        break;
                    case ".cs":
                        if (fileInfo.Name.EndsWith("enum.cs", StringComparison.OrdinalIgnoreCase))
                        {
                            search = new EnumSearch();
                        }
                        else
                        {
                            search = new CsSearch();
                        }
                        break;
                    default:
                        if (fileInfo.Name.EndsWith(".metadata.config", StringComparison.OrdinalIgnoreCase))
                        {
                            search = new JsSearch();
                        }
                        break;
                }

                if (search != null)
                {
                    usedLangs.AddRange(search.Search(fileInfo));
                }
            }

            foreach (var fileInfo in dir.GetDirectories())
            {
                //临时修改的文件不要放进来
                if (fileInfo.FullName.EndsWith(@"src\05_售楼修改"))
                {
                    continue;
                }
                GetLange(fileInfo.FullName);
            }
        }

        /// <summary>
        /// 多语言js处理
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        private List<string> LangJsDo(FileInfo fileInfo)
        {
            StringBuilder textSb = new StringBuilder();
            List<string> langKey = new List<string>();
            //循环js、查找js中的多语言:{##[lang:PriceMng_BtmMng_000001]##}
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex(@"(.*{##\[lang:).*(\]##})");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    textSb.Append(str + "\r\n");
                    continue;
                }

                var jsUseName = str.Split(new string[] { ":" }, StringSplitOptions.None).FirstOrDefault();
                if (jsUseName == null)
                {
                    textSb.Append(str + "\r\n");
                    continue;
                }
                jsUseName = jsUseName.Trim();
                //检查是否被使用
                var isUsed = CheckLangJsIsUsed(jsUseName, SrcPath);
                if (isUsed == true)
                {
                    var value = match.Groups[0].ToString();
                    if (string.IsNullOrEmpty(value) == true)
                    {
                        break;
                    }
                    var re = value.Split(new string[] { @"##[lang:" }, StringSplitOptions.None).Last().Replace(@"]##}", "");
                    langKey.Add(re.Trim());
                    textSb.Append(str + "\r\n");
                }
            }
            //修改文件
            FileHelper.WriteFile(fileInfo.FullName, textSb.ToString());
            return langKey;
        }

        /// <summary>
        /// 检查多语言中名称的文字是否被使用
        /// </summary>
        /// <param name="usedName"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool CheckLangJsIsUsed(string usedName, string path)
        {
            var dir = new DirectoryInfo(path);
            var isUsed = false;
            foreach (var fileInfo in dir.GetFiles())
            {
                switch (fileInfo.Extension)
                {
                    case ".js":
                        //如果是多语言文件，跳过
                        if (fileInfo.Name.EndsWith("LangRes.js"))
                        {
                            break;
                        }
                        else
                        {
                            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
                            {
                                if (str.IndexOf(usedName) > -1)
                                {
                                    isUsed = true;
                                    break;
                                }
                            }
                        }
                        break;
                }

                if (isUsed == true)
                {
                    break;
                }
            }

            if (isUsed == true)
            {
                return true;
            }

            foreach (var fileInfo in dir.GetDirectories())
            {
                isUsed = CheckLangJsIsUsed(usedName, fileInfo.FullName);
                if (isUsed == true)
                {
                    break;
                }
            }

            return isUsed;
        }

        private List<ListDic> replaceDictionary = new List<ListDic>();


        List<string> LoadFileType = new List<string>()
        {
            ".cs",".js",".config"
        };
        /// <summary>
        /// 加载所有文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="langKey"></param>
        private void LoadAllFile(string path)
        {
            var dir = new DirectoryInfo(path);
            foreach (var fileInfo in dir.GetFiles())
            {
                if (LoadFileType.Any(t => t == fileInfo.Extension.ToLower()))
                {
                    var encoding = EncodingType.GetEncoding(fileInfo.FullName);
                    replaceDictionary.Add(new ListDic
                    {
                        Key = fileInfo.FullName,
                        Value = System.IO.File.ReadAllText(fileInfo.FullName, encoding)
                    });
                }
            }

            foreach (var fileInfo in dir.GetDirectories())
            {
                LoadAllFile(fileInfo.FullName);
            }
        }

        /// <summary>
        /// 将XML字符串反序列化为对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="xml">XML字符</param>
        /// <returns></returns>
        public static T DeserializeToObject<T>(string xml)
        {
            T myObject;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xml);
            myObject = (T)serializer.Deserialize(reader);
            reader.Close();
            return myObject;
        }
    }

    internal interface ILangeSearch
    {
        List<string> Search(FileInfo fileInfo);
    }

    internal class JsSearch : ILangeSearch
    {
        public List<string> Search(FileInfo fileInfo)
        {
            List<string> langKey = new List<string>();
            //循环js、查找js中的多语言:{##[lang:PriceMng_BtmMng_000001]##}
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex(@"(.*{##\[lang:).*(\]##})");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    continue;
                }
                var value = match.Groups[0].ToString();
                if (string.IsNullOrEmpty(value) == true)
                {
                    break;
                }

                var re = value.Split(new string[] { @"##[lang:" }, StringSplitOptions.None).Last().Replace(@"]##}", "");
                langKey.Add(re.Trim());
            }

            return langKey;
        }
    }

    internal class EnumSearch : ILangeSearch
    {
        public List<string> Search(FileInfo fileInfo)
        {
            List<string> langKey = new List<string>();
            //枚举: [MultiLanguage("L0011_000329")]
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex(@"(.*\[MultiLanguage\().*(\)\])");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    continue;
                }
                var value = match.Groups[0].ToString();
                if (string.IsNullOrEmpty(value) == true)
                {
                    break;
                }

                var re = value.Split(new string[] { "MultiLanguage(\"" }, StringSplitOptions.None).Last().Replace("\")]", "");
                langKey.Add(re.Trim());
            }

            return langKey;
        }
    }

    internal class CsSearch : ILangeSearch
    {
        public List<string> Search(FileInfo fileInfo)
        {
            List<string> langKey = new List<string>();
            //后台1：public static string L001100110851PzExportErro => "L0011_00110851_PzExportErro".Translate();
            //后台2："L0011_00110851_PzExportErro".Translate(); 
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex("(\").*(\\.Translate\\(\\))");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    continue;
                }
                var value = match.Groups[0].ToString();
                if (string.IsNullOrEmpty(value) == true)
                {
                    break;
                }

                var re = value.Split(new string[] { ".Translate()" }, StringSplitOptions.None).First().Replace("\"", "");
                langKey.Add(re.Trim());
            }

            return langKey;
        }
    }
}