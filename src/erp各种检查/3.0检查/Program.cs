﻿using _3._0检查.Model;
using Mysoft.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace _3._0检查
{
    class Program
    {
        static void Main(string[] args)
        {
            LoadAppSettings();

            List<CheckResult> results = new _3._0检查.Business.检查domain的非internal方法.Main().Fx();

            if (results.Count > 0)
            {
                //创建html
                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                sb.Append("<tr><td>标题</td><td>内容</td></tr>");
                foreach (var item in results)
                {
                    sb.Append($"<tr><td>{item.Title}</td><td>{item.Content}</td></tr>");
                }
                sb.Append("</table>");
                //文件存放位置
                FileHelper.WriteFile(Model.AppSettings.CheckSys.Instance.FileSavePath, sb.ToString());

                //发送邮件
                SendMailHelper.Send(new SendMainDto
                {
                    SendEmail = Model.AppSettings.MailSetting.Instance.SendEmail,
                    SendEmailPwd = Model.AppSettings.MailSetting.Instance.SendEmailPwd,
                    ToEmails = Model.AppSettings.MailSetting.Instance.ToEmails,
                    IsBodyHtml = Model.AppSettings.MailSetting.Instance.IsBodyHtml,
                    Port = Model.AppSettings.MailSetting.Instance.Port,
                    SMTP = Model.AppSettings.MailSetting.Instance.SMTP,
                    EnableSsl = Model.AppSettings.MailSetting.Instance.EnableSsl,
                    Subject = "售楼-销售变更业务单元检测",
                    Body = string.IsNullOrEmpty(sb.ToString()) ? "无问题" : sb.ToString()
                });
            }
        }


        private static void LoadAppSettings()
        {
            var config = new ConfigurationBuilder()
                  .AddJsonFile("appSettings.json", optional: true, reloadOnChange: false)
                  .Build();
            Model.AppSettings.MailSetting.Instance = config.GetSection(Model.AppSettings.MailSetting.MailSettingKey).Get<Model.AppSettings.MailSetting>();
            Model.AppSettings.CheckSys.Instance = config.GetSection(Model.AppSettings.CheckSys.CheckSysKey).Get<Model.AppSettings.CheckSys>();
        }
    }
}
