﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace _3._0检查.Model
{
    public class FileDto
    {
        public string FilePath { get; set; }

        public string FileName { get; set; }

        public FileInfo FileInfo { get; set; }
    }
}
