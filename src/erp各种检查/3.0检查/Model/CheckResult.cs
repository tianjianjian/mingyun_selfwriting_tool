﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3._0检查.Model
{
    /// <summary>
    /// 检查结果
    /// </summary>
    internal class CheckResult
    {
        public string Title { get; set; }

        public string Content { get; set; }


        public string HtmlContent { get; set; }
    }
}
