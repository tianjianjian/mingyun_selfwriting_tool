﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace _3._0检查.Model
{
    [XmlRoot(ElementName = "Langs")]
    public class LangModel
    {
        [XmlElement(ElementName = "List")]
        public List List { get; set; }

        [XmlElement("Language")]
        public string Language { get; set; }
    }

    public class List
    {
        [XmlElement(ElementName ="string")]
        public StringKey[] StringKeyList { get; set; } 
    }

    public class StringKey
    { 
        [XmlAttribute("Key")]
        public string Key { get; set; }


        [XmlText]
        public string Text { get; set; }
    }
}