﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3._0检查.Model.AppSettings
{
    public class CheckSys
    {
        public static readonly string CheckSysKey = "CheckSys";

        public static CheckSys Instance { get; set; }

        /// <summary>
        /// 系统code
        /// </summary>
        public string Xtcode { get; set; }

        /// <summary>
        /// GitUrl
        /// </summary>
        public string GitUrl { get; set; }

        /// <summary>
        /// Feature
        /// </summary>
        public string Feature { get; set; }

        /// <summary>
        /// CheckDll
        /// </summary>
        public string CheckDll { get; set; }

        /// <summary>
        /// FileSavePath
        /// </summary>
        public string FileSavePath { get; set; }
    }
}
