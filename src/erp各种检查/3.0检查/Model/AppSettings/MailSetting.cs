﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3._0检查.Model.AppSettings
{
    public class MailSetting
    {
        public static readonly string MailSettingKey = "MailSetting";

        public static MailSetting Instance { get; set; }

        /// <summary>
        /// 发送人邮箱
        /// </summary>
        public string SendEmail { get; set; }

        /// <summary>
        /// 发送人密码
        /// </summary>
        public string SendEmailPwd { get; set; }

        /// <summary>
        /// 接送人邮箱
        /// </summary>
        public List<string> ToEmails { get; set; }

        /// <summary>
        /// 内容是否是html
        /// </summary>
        public bool IsBodyHtml { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// SMTP
        /// </summary>
        public string SMTP { get; set; }

        /// <summary>
        /// EnableSsl
        /// </summary>
        public bool EnableSsl { get; set; }
    }
}
