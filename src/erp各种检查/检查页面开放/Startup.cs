﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(检查页面开放.Startup))]
namespace 检查页面开放
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
