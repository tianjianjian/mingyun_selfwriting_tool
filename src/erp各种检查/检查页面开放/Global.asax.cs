﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using 检查页面开放.Models;

namespace 检查页面开放
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public class FilterConfig
        {
            public static void RegisterGlobalFilters(GlobalFilterCollection filters)
            {
                //filters.Add(new CustomErrorAttribute());
            }
        }

        //public class CustomErrorAttribute : HandleErrorAttribute
        //{
        //    public override void OnException(ExceptionContext filterContext)
        //    {
        //        var context = filterContext.HttpContext;
        //        var response = filterContext.HttpContext.Response;
        //        if (filterContext.Exception is BusinessLogicException)
        //        {
        //            HttpException httpException = filterContext.Exception as HttpException;
        //            response.StatusCode = httpException != null ? httpException.GetHttpCode() : 500;
        //            response.TrySkipIisCustomErrors = true;

        //            context.Response.Clear();
        //            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            context.Response.ContentType = "text/plain";

        //            context.Response.Write(filterContext.Exception.Message);
        //            return;
        //        }
        //        base.OnException(filterContext);


        //    }
        //}
    }
}
