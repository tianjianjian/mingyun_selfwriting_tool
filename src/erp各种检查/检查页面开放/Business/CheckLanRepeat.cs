﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using 检查页面开放.Models;
using 检查页面开放.Utility;

namespace 检查页面开放.Business
{
    public class ErrorLang
    {
        public string MetaName { get; set; }

        private List<string> _keyList = new List<string>();
        public List<string> KeyList
        {
            get { return _keyList; }
            set { this._keyList = value; }
        }
    }
    /// <summary>
    /// 检查代码中的多语言有没有未翻译的
    /// </summary>
    public class CheckLanRepeat
    {
        private string path = "";
        private List<ErrorLang> errorLangList;

        public CheckLanRepeat(string path)
        {
            this.path = path;
            errorLangList = new List<ErrorLang>();
        }
        public List<ErrorLang> Check()
        {
            DirectoryInfo dic = new DirectoryInfo(path);
            foreach (var fileInfo in dic.GetFiles())
            {
                string str2 = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.ASCII);
                //判断是否是销售系统
                if (str2.IndexOf("application=\"0011\"", StringComparison.OrdinalIgnoreCase) == -1)
                {
                    continue;
                }
                LangModel functionModel = XmlUtility.DeserializeToObject<LangModel>(str2);
                var reapeatLang = functionModel.List.StringKeyList.GroupBy(t => t.Key).Where(t => t.Count() > 1);
                 
                if (reapeatLang.Any())
                {
                    List<string> keyList = new List<string>();
                    foreach (var repeat in reapeatLang)
                    {
                        keyList.Add(repeat.Key);
                    }

                    errorLangList.Add(new ErrorLang
                    {
                        MetaName = fileInfo.Name,
                        KeyList = keyList
                    });
                }
            }
            return errorLangList;
        }
    }
}