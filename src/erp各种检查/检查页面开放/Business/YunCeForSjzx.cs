﻿using Mysoft.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mysoft.ModelAnalysis.Utility;
using 检查页面开放.Models;
using 检查页面开放.Controllers;
using Newtonsoft.Json;

namespace 检查页面开放.Business
{
    public class YunCeForSjzx
    {
        public string Search()
        {
            string jsonStr = HttpHelper.Http("http://10.5.10.162:50780/test/GetInterfaceCoverage");
            List<SjzxModel> sjztModel = jsonStr.FromJson<List<SjzxModel>>();


            List<MethodInfo> allMethod = new List<MethodInfo>();
            foreach (var sjzxModel in sjztModel)
            {
                foreach (var item in sjzxModel.Method)
                {
                    allMethod.Add(new MethodInfo
                    {
                        ClassFullName = sjzxModel.ClassFullName,
                        MethodName = item.MethodName,
                        ModelName="数据中心",
                        IsOpen = sjzxModel.IsPublic,
                        IsPublic = false
                    });
                }
            }
            List<InterFaceResult> interFaceResults = new List<InterFaceResult>();
            var zt = new InterFaceResult
            {
                Remark = "整体",
                Count = allMethod.Count,
                MethodList = allMethod
            };
            zt.Fgl = zt.YcCount * 1.00 / zt.Count * 100;
            interFaceResults.Add(zt);

            var nbjk = new InterFaceResult
            {
                Remark = "业务单元内部接口",
                Count = allMethod.Count(t => t.IsPublic.GetValueOrDefault() == false),
                YcCount = allMethod.Count(t => t.IsPublic.GetValueOrDefault() == false && t.YCIsCover == true),
                MethodList = allMethod.FindAll(t => t.IsPublic.GetValueOrDefault() == false)
            };
            nbjk.Fgl = nbjk.YcCount * 1.00 / nbjk.Count * 100;
            interFaceResults.Add(nbjk);

            YcResult ycResult = new YcResult();
            ycResult.InterFaceResults = interFaceResults;

            return ycResult.ToJson();
        }
    }



    [Serializable]
    public class SjzxModel
    {
        [JsonProperty("ClassFullName")]
        public string ClassFullName { get; set; }

        [JsonProperty("sub_ticket")]
        public string Sub_ticket { get; set; }

        [JsonProperty("Method")]
        public List<SjzxMethod> Method { get; set; }

        [JsonProperty("IsPublic")]
        public bool IsPublic { get; set; }
    }

    public class SjzxMethod
    {
        [JsonProperty("MethodName")]
        public string MethodName { get; set; }

    }
}