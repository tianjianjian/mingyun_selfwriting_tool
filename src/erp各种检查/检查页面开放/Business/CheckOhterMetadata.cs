﻿using Mysoft.ModelAnalysis.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business
{
    /// <summary>
    /// 检查售楼是否有其他元数据
    /// </summary>
    public class CheckOhterMetadata
    {
        private string path = PathConst.YsjAddress;
        List<string> notSlMeta = new List<string>();

        public List<string> ReadFile()
        {
            Read(path);
            return notSlMeta;
        }

        private void Read(string pathUrl)
        {
            //全开放（图表） isAllowEdit="true" editMode="AllowAll" isRevisedId="1"
            //全开放 isAllowEdit="true" editMode="AllowAll">  或者  isAllowEdit="true">
            //部分开放isAllowEdit="true" editMode="AllowAdd">
            //不开放  isAllowEdit="false" editMode="AllowAll">  isAllowEdit="false" editMode="AllowAll">
            //ASPX页面  pageType="1" 
            System.IO.DirectoryInfo dic = new DirectoryInfo(pathUrl);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Extension != ".config")
                    continue;
                string str2 = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.ASCII);
                //判断是否是销售系统
                if (str2.IndexOf("application=\"0011\"", StringComparison.OrdinalIgnoreCase) == -1)
                {
                    notSlMeta.Add(fileInfo.FullName);
                    continue;
                }
            }

            foreach (var dir in dic.GetDirectories())
            {
                Read(dir.FullName);
            }
        }
    }
}