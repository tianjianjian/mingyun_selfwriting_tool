﻿using Mysoft.ModelAnalysis.Utility;
using ReadFileNames.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using 检查页面开放.Models;
using 检查页面开放.Utility;

namespace 检查页面开放.Business
{
    /// <summary>
    /// 检查开放清单
    /// </summary>
    public class ChekOpenList
    {
        private string path = PathConst.YsjFunctionPageAddress;
        public YmModel ReadFile()
        {
            YmModel ymModel = new YmModel();
            //全开放（图表） isAllowEdit="true" editMode="AllowAll" isRevisedId="1"
            //全开放 isAllowEdit="true" editMode="AllowAll">  或者  isAllowEdit="true">
            //部分开放isAllowEdit="true" editMode="AllowAdd">
            //不开放  isAllowEdit="false" editMode="AllowAll">  isAllowEdit="false" editMode="AllowAll">
            //ASPX页面  pageType="1" 
            System.IO.DirectoryInfo dic = new DirectoryInfo(path);
            List<string> expectList = DataSourceHelp.GetTxtList(PathConst.OpenExpectList);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Extension != ".config")
                    continue;
                string str2 = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.ASCII);
                //判断是否是销售系统
                if (str2.IndexOf("application=\"0011\"", StringComparison.OrdinalIgnoreCase) == -1)
                {
                    continue;
                }
                ymModel.TotalCount++;
                if(fileInfo.Name== "0198d376-0626-11e6-8289-7427eac44718.metadata.config")
                {
                    var l = 1;
                }
                //isAllowEdit="false" editMode="AllowAll"
                if (str2.IndexOf("isAllowEdit=\"true\" editMode=\"AllowAll\"", StringComparison.OrdinalIgnoreCase) != -1
                                 || str2.IndexOf("isAllowEdit=\"true\"", StringComparison.OrdinalIgnoreCase) != -1)
                {//全开放
                    ymModel.QkfCount++;
                }
                else if (str2.IndexOf("isAllowEdit=\"true\" editMode=\"AllowAdd\">", StringComparison.OrdinalIgnoreCase) != -1)
                {//部分开放
                    ymModel.BfCount++;
                }
                else if (str2.IndexOf("pageType=\"1\"", StringComparison.OrdinalIgnoreCase) != -1)
                {//ASPX页面
                    continue;
                    //ymModel.AspxCount++;
                    //FunctionModel functionModel = XmlUtility.DeserializeToObject<FunctionModel>(str2);
                    //ymModel.ApsxBkfList.Add(functionModel.PageName + "&" + functionModel.Title + "&" + fileInfo.Name);

                }

                else
                {//不开放
                    FunctionModel functionModel = XmlUtility.DeserializeToObject<FunctionModel>(str2);
                    var name = functionModel.PageName + "&" + functionModel.Title + "&" + fileInfo.Name;
                    if (expectList.Contains(name.Trim()))
                    {
                        continue;
                    }
                    ymModel.BkfCount++;
                    ymModel.PtBkfList.Add(name);
                }
            }
            return ymModel;
        }
    }
}