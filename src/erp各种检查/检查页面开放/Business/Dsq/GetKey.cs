﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace 检查页面开放.Business.Dsq
{
    public class GetKey
    {
        public static string Get(string code)
        {
            return "CheckDsq_" + code.ToLower();
        }

        public static string GetInstanceKey(FileInfo fileInfo)
        {
            string fullName = fileInfo.FullName.ToLower();
            if (fullName.EndsWith(".cs"))
            {
                return CsCheck.ServiceKey;
            }
            if (fullName.EndsWith(".js"))
            {
                return JsCheck.ServiceKey;
            }
            if (fullName.EndsWith(".metadata.config".ToLower()))
            {
                return MetadataCheck.ServiceKey;
            }
            if (fullName.EndsWith(".XmlCommand.Config".ToLower()))
            {
                return XmlCommandCheck.ServiceKey;
            }
            return OtherCheck.ServiceKey;
        }
    }
}