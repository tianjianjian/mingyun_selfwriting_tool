﻿using Mysoft.ModelAnalysis.Utility;
using Mysoft.Utility.InstanFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business.Dsq
{
    public class CheckDsq
    {
        private readonly List<CheckDsqDTO> chekResult;
        public CheckDsq()
        {
            Factory.Register<CsCheck>(CsCheck.ServiceKey);
            Factory.Register<JsCheck>(JsCheck.ServiceKey);
            Factory.Register<MetadataCheck>(MetadataCheck.ServiceKey);
            Factory.Register<XmlCommandCheck>(XmlCommandCheck.ServiceKey);
            Factory.Register<OtherCheck>(OtherCheck.ServiceKey);
            chekResult = new List<CheckDsqDTO>();
        }

        private List<string> expectList = new List<string> { ".dll", ".css", ".pdb", ".xml", ".cmd", ".sln", ".editorconfig", ".exe", ".png" };
        private List<string> expectFolder = new List<string>() { @"src\build", ".UnitTest", "03 升级脚本", "04 翻倍脚本", @"src\.vs", @"00_根目录\Log", @"00_根目录\bin\lib", @"src\99 packages", @"00_根目录\bin", @"obj\Debug", @"src\packages" };
        /// <summary>
        /// 多时区排出文件
        /// </summary>
        private List<string> ExpectFileList = DataSourceHelp.GetTxtList(PathConst.DsqExpectFile);
        /// <summary>
        /// 检查
        /// </summary>
        public void Check(string path)
        {
            DirectoryInfo theFolder = new DirectoryInfo(path);
            foreach (var info in theFolder.GetFiles())
            {
                if (expectList.Contains(info.Extension))
                {
                    continue;
                }
                if (IsExpectFile(info))
                {
                    continue;
                }
                var checkInstance = ((ICheckDsq)Factory.GetInstance(GetKey.GetInstanceKey(info)));
                chekResult.AddRange(checkInstance.Check(info));
            }

            //是否是根目录
            var isGml = theFolder.FullName.EndsWith(@"src\00_根目录");
            foreach (var childrenD in theFolder.GetDirectories())
            {
                if (IsExpectFolder(childrenD))
                {
                    continue;
                }

                if (isGml && (childrenD.FullName.EndsWith("_metadata") || childrenD.FullName.EndsWith("Slxt")) == false)
                {
                    continue;
                }
                Check(childrenD.FullName);
            }
        }

        public List<CheckDsqDTO> GetResult()
        {
            return chekResult;
        }

        /// <summary>
        /// 是否是排除的文件夹
        /// </summary>
        /// <returns></returns>
        private bool IsExpectFolder(DirectoryInfo childrenD)
        {
            foreach (var item in expectFolder)
            {
                if (childrenD.FullName.EndsWith(item))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsExpectFile(FileInfo info)
        {
            foreach (var item in ExpectFileList)
            {
                if (info.FullName.EndsWith(item))
                {
                    return true;
                }
            }
            return false;
        }
    }
}