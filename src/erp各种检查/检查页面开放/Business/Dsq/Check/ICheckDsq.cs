﻿using Mysoft.Utility.InstanFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business.Dsq
{
    public interface ICheckDsq : IService
    {
        List<CheckDsqDTO> Check(FileInfo fileInfo);
    }
}