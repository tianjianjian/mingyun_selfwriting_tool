﻿using Mysoft.Utility.InstanFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business.Dsq
{
    public class XmlCommandCheck : ICheckDsq
    {
        public static string ServiceKey = GetKey.Get("XmlCommand");
        private readonly List<string> CheckList = new List<string>
        {
            "23:59:59",
            "CONVERT(NVARCHAR(10),",
            "Day(",
            "getDate()"
        };

        public List<CheckDsqDTO> Check(FileInfo info)
        {
            return Common.Check(info, CheckList);
        }

    }
}