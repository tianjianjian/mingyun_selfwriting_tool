﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business.Dsq
{
    public class Common
    {
        private static readonly string SysGetDate = "fn_timezone_getdate()";
        public static List<CheckDsqDTO> Check(FileInfo info, List<string> checkList)
        {
            List<CheckDsqDTO> re = new List<CheckDsqDTO>();
            string text = new StreamReader(info.FullName).ReadToEnd();

            string line;
            using (StringReader srr = new StringReader(text))
            {
                while ((line = srr.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line) == false && line.IndexOf(PathConst.DsqHl) == -1)
                    {
                        Compare(info, checkList, re, line);
                    }
                }
            }
            return re;
        }

        private static void Compare(FileInfo info, List<string> checkList, List<CheckDsqDTO> re, string line)
        {
            foreach (var checkStr in checkList)
            {
                //fn_timezone_getdate()	不应被匹配	匹配规则：getdate()
                if (checkStr.ToLower() == "getdate()")
                {
                    if (line.IndexOf(SysGetDate, StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        continue;
                    }
                }
                else if (checkStr.ToLower() == "CONVERT(NVARCHAR(10),".ToLower())
                {
                    //已经用了fn_timezone_getdate就不处理
                    if (line.IndexOf(SysGetDate, StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        continue;
                    }
                }
                else if (line.IndexOf(checkStr, StringComparison.OrdinalIgnoreCase) > -1)
                {
                    re.Add(new CheckDsqDTO
                    {
                        FileName = info.FullName,
                        Content = line
                    });

                }
            }
        }
    }
}