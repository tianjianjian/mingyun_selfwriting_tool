﻿using Mysoft.Utility.InstanFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business.Dsq
{
    public class CsCheck : ICheckDsq
    {
        public static string ServiceKey = GetKey.Get("cs");

        private readonly List<string> CheckList = new List<string>
        {
            "Convert.ToDateTime",
            "DateTime.TryParse",
            "DateTime.TryParseExact",
            "DateTime.ToLocalTime",
            "DateTime.Now",
            "new DateTime"
        };

        public List<CheckDsqDTO> Check(FileInfo info)
        {
            return Common.Check(info, CheckList);
        }
    }
}