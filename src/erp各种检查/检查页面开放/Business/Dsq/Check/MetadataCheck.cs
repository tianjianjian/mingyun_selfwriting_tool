﻿using Mysoft.Utility.InstanFactory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business.Dsq
{
    public class MetadataCheck : ICheckDsq
    {
        public static string ServiceKey = GetKey.Get("Metadata");

        private readonly List<string> CheckList = new List<string>
        {
            "new Date()",
            "getDate()",
            "23:59:59",
            "CONVERT(NVARCHAR(10),",
            "Day(",
            "getDate()"
        };

        public List<CheckDsqDTO> Check(FileInfo info)
        {
            string text = new StreamReader(info.FullName).ReadToEnd();
            if (text.IndexOf($"application=\"{PathConst.SysCode}\"") == -1)
            {
                return new List<CheckDsqDTO>();
            }
            return Common.Check(info, CheckList);
        }

    }
}