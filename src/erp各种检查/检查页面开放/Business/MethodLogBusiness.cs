﻿using EasyFramework.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using 检查页面开放.Models;

namespace 检查页面开放.Business
{
    public class MethodLogBusiness : ObjectDAO<MethodLog>
    {
        public MethodLog Get(int id)
        {
            return base.Get(id);
        }

        public void Add(MethodLog methodLog)
        {
            if (methodLog == null)
            {
                throw new ArgumentException(nameof(methodLog));
            }
            base.Insert(methodLog);
        }

        public List<MethodLog> Query()
        {
            var sql = "SELECT * FROM dbo.MethodLog ORDER BY CreateTime DESC";
            return base.Query(sql).ToList<MethodLog>();
        }

        public virtual bool IsExiste(string name)
        {
            var sql = "SELECT * FROM dbo.MethodLog where Name=@Name";

            return base.Query(sql, new System.Data.SqlClient.SqlParameter[] {
                new System.Data.SqlClient.SqlParameter("@Name",name)
            }).Count() > 0;
        }
    }
}