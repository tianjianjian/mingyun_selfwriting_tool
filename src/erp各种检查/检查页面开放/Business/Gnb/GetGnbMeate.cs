﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Mysoft.Utility;

namespace 检查页面开放.Business.Gnb
{
    /// <summary>
    /// 获取功能包中的元数据
    /// </summary>
    public class GetGnbMeate
    {
        /// <summary>
        /// 入口
        /// </summary>
        /// <returns></returns>
        public string GetMateList(string gitPath, string feature)
        {
            GitHelper giteHelper = new GitHelper();
            //获取代码
            giteHelper.Clone(gitPath, feature, FeatureTypeEnum.Feature);
            //获取元数据
            string path = giteHelper.GetRootPath();
            List<string> meateList = GetMates(path);
            //异步删除代码 
            new Task(() =>
            {
                Thread.Sleep(2000);
                PathHelper.DeletePath(path);
            }).Start();
            return meateList.ToJson();
        }

        private List<string> GetMates(string path)
        {
            List<string> list = new List<string>();
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            foreach (var fileInfo in directoryInfo.GetFiles())
            {
                var fullName = fileInfo.FullName;
                //- "_metadata/AppForm/03465a3c-36e6-4f8e-a6dc-0b8ceb74d882.metadata.config"
                if (fullName.EndsWith(".metadata.config",StringComparison.OrdinalIgnoreCase))
                {

                    list.Add($"- \"_metadata/{fileInfo.Directory.Name}/{fileInfo.Name}\"");
                }
            }

            foreach (var directory in directoryInfo.GetDirectories())
            {
                list.AddRange(GetMates(directory.FullName));
            }

            return list;
        }
    }
}