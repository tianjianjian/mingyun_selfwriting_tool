﻿using Mysoft.ModelAnalysis.Models;
using Mysoft.ModelAnalysis.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace 检查页面开放.Business
{
    /// <summary>
    /// 元数据的操作
    /// </summary>
    public class MetaDataOp
    {
        /// <summary>
        /// 获取所有元数据实体
        /// </summary>
        /// <returns></returns>
        public static List<MetadataEntity> GetMetadataEntities()
        {
            FileStream fs = null;
            List<MetadataEntity> metadataEntityList = new List<MetadataEntity>();
            string path = DataSourceHelp.GetEntityMetadataPath(); //@"E:\工作\GitCode2\src\00_根目录\_metadata\Entity\";
            DirectoryInfo root = new DirectoryInfo(path);
            
            string errorFile="";
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(MetadataEntity));
                foreach (FileInfo f in root.GetFiles())
                {
                    errorFile = f.FullName;
                       fs = f.OpenRead();
                    MetadataEntity metadataEntity = (MetadataEntity)xs.Deserialize(fs);
                    fs.Close();
                    if (metadataEntity.Application == "0011")
                    {
                        metadataEntity.FullName = f.FullName;
                        metadataEntityList.Add(metadataEntity);
                    }
                }
                return metadataEntityList;
            }
            catch (Exception ex)
            {

                if (fs != null)

                    fs.Close();

                throw new Exception("文件："+ errorFile+ex.StackTrace);

            }
        }
    }
}