﻿using Mysoft.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mysoft.ModelAnalysis.Utility;
using 检查页面开放.Models;

namespace 检查页面开放.Business
{
    /// <summary>
    /// 云测接口现状
    /// </summary>
    public class YunCeBusiness
    {
        private YunCeModel yunCeModel=new YunCeModel();
        private readonly GetAppApi _getAppApi = new GetAppApi();
        private List<AppDataSource> _appDataSource;

        public List<Model> Search(string ycUrl, List<AppDataSource> appDataSource)
        {
            //string jsonStr = HttpHelper.Http(ycUrl);
            //yunCeModel = jsonStr.FromJson<YunCeModel>();
            this._appDataSource = appDataSource;

            return Match();
        }

        private List<Model> Match()
        {
            List<Model> models = _getAppApi.GetModels(_appDataSource);
            if (yunCeModel.IsSuccess == false)
            {
                return models;
            }
            //获取产品的所有接口

            //用云测和产品的接口进行匹配，检查是否一致


            foreach (var model in models)
            {
                foreach (var method in model.MethodList)
                {
                    var route = $"/{method.ClassFullName}/{method.MethodName}"; 
                    //"route":"/pub/Mysoft.Slxt.ProjectPrep.Interfaces.IRoomGeneratePublicService/QueryRooms",
                    if (yunCeModel.Result.Any(t => t.Route.Contains(route)))
                    {
                        method.YCIsCover = true;
                    }
                }
            }
            return models;
        }
    }
}