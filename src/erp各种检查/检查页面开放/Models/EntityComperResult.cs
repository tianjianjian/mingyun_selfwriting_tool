﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mysoft.ModelAnalysis.Models
{
    public class EntityComperResult
    {
        /// <summary>
        /// 表名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否存在
        /// </summary>
        public bool IsExists { get; set; }
        /// <summary>
        /// 实体多出的字段
        /// </summary>
        public List<string> EntityMoreAttribute { get; set; }
        /// <summary>
        /// 元数据多出的字段
        /// </summary>
        public List<string> MetadataMoreAttribute { get; set; }
    }
}