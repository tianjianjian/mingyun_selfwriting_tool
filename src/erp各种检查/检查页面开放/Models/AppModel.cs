﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    /// <summary>
    /// 方法信息
    /// </summary>
    /// <remarks>
    /// 例如：Mysoft.Slxt.FinanceMng
    /// </remarks>
    [Serializable]
    public class MethodInfo
    {
        public string DllName { get; set; }


        public string ModelName { get; set; }

        public string ClassName { get; set; }

        /// <summary>
        /// 全称(包括命名空间)
        /// </summary>
        /// <remarks>
        /// c.FullName
        /// </remarks>
        public string ClassFullName { get; set; }

        public string MethodName { get; set; }

        public string MethodRemark { get; set; }

        /// <summary>
        /// 是否对外开放（第三方）
        /// </summary>
        public bool? IsOpen { get; set; }

        /// <summary>
        /// 是否对外开放(第三方、跨模块)
        /// </summary>
        public bool? IsPublic { get; set; }

        public List<MyParameterInfo> Params { get; set; }

        /// <summary>
        /// 云测平台是否覆盖
        /// </summary>
        public bool? YCIsCover { get; set; }
    }

   /// <summary>
   /// 方法参数
   /// </summary>
    [Serializable]
    public class MyParameterInfo
    {
        public string TypeName { get; set; }

        public void NotImplementedException()
        {

        }
    }

    /// <summary>
    /// 方法差异
    /// </summary>
    [Serializable]
    public class MethodDiff
    {
        public MethodInfo Method1 { get; set; }

        public MethodInfo Method2 { get; set; }

        public bool IsDiff { get; set; }

        public DiffEnum DiffType { get; set; }
    }

    /// <summary>
    /// 模块
    /// </summary>
    [Serializable]
    public class Model
    {
        public string Name { get; set; }

        public List<MethodInfo> MethodList { get; set; }
    }

    /// <summary>
    /// 模块差异
    /// </summary>
    [Serializable]
    public class ModelDiff
    {
        public Model Model { get; set; }

        public List<MethodDiff> MethodDiffs { get; set; }
    }

    /// <summary>
    /// 差异类型
    /// </summary>
    public enum DiffEnum
    {
        //删除
        Deleted = 1,

        //判断参数个数、类型是否调整
        Params = 2,

        //判断内容行数是否调整
        Content = 3,

        //判断注释是否调整
        Remarks = 4,

        //新增
        Add = 5
    }
}