﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace 检查页面开放.Models
{
    [XmlRoot(ElementName = "ArrayOfXmlCommand")]
    public class ArrayOfXmlCommand
    {
        [XmlElement(ElementName = "XmlCommand")]
        public List<XmlCommand> CommandList { get; set; }
    }

    public class XmlCommand
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
    }
}