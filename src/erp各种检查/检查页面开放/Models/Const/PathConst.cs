﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    public class PathConst
    {
        /// <summary>
        /// 售楼全路径:C:\公司\git\slxt2.0\src
        /// </summary>
        public static readonly string SlAddress = ConfigurationManager.AppSettings["SlAddress"];


        /// <summary>
        /// 售楼全路径:C:\公司\git\slxt2.0\src\00_根目录
        /// </summary>
        public static readonly string SlRootAddress = Path.Combine(SlAddress, @"00_根目录");

        /// <summary>
        /// 售楼前端路径：:C:\公司\git\slxt2.0\src\00_根目录\Slxt
        /// </summary>
        public static readonly string SlJsPath = Path.Combine(SlAddress, @"00_根目录\Slxt");

        /// <summary>
        /// 获取所有元数据地址:E:\工作\GIT\slxt\src\00_根目录\_metadata
        /// </summary>
        public static readonly string YsjAddress = Path.Combine(SlAddress, @"00_根目录\_metadata");

        /// <summary>
        /// 获取所有元数据地址:E:\工作\GIT\slxt\src\00_根目录\_metadata
        /// </summary>
        public static readonly string YsjAppFormAddress = Path.Combine(YsjAddress, "AppForm");


        /// <summary>
        /// 获取所有元数据地址:E:\工作\GIT\slxt\src\00_根目录\_metadata\FunctionPage
        /// </summary>
        public static readonly string YsjFunctionPageAddress = Path.Combine(YsjAddress, @"FunctionPage");

        /// <summary>
        /// E:\工作\GIT\slxt\src\00_根目录
        /// </summary>
        public static readonly string RootPath = Path.Combine(SlAddress, @"00_根目录");

        /// <summary>
        /// E:\工作\GIT\slxt\src\00_根目录\Bin
        /// </summary>
        public static readonly string BinPath = Path.Combine(SlAddress, @"00_根目录\Bin");


        /// <summary>
        /// App_Data\ExCheckAppClass.txt 页面开放排出清单
        /// </summary>
        public static readonly string OpenExpectList = @"App_Data\页面开放排出清单.txt";

        /// <summary>
        /// 多时区排出清单
        /// </summary>
        public static readonly string DsqExpectFile = @"App_Data\DsqExpectFile.txt";

        /// <summary>
        /// markdown生成地址
        /// </summary>
        public static readonly string MdFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"Md\doc-website-master\doc\Md");

        /// <summary>
        /// 分库
        /// </summary>
        public static readonly string Fk = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"App_Data\Fk.xml");


        /// <summary>
        /// 售楼的AppDll
        /// </summary>
        /// <remarks>
        /// 例如：
        /// <ModelDlls>
        /// <Item>Mysoft.Slxt.Common.dll</Item>
        /// <Item>Mysoft.Slxt.ProjectPrep.dll</Item>
        /// <Item>Mysoft.Slxt.PriceMng.dll</Item>
        /// <Item>Mysoft.Slxt.TradeMng.dll</Item>
        /// <Item>Mysoft.Slxt.FinanceMng.dll</Item>
        /// <Item>Mysoft.Slxt.SaleService.dll</Item>
        /// <Item>Mysoft.Slxt.OpportunityMng.dll</Item>
        /// <Item>Mysoft.Slxt.Scyx.dll</Item> 
        /// </ModelDlls> 
        /// </remarks>
        public static readonly string AppDataSouce = "/App_Data/AppDataSouce.xml";

        /// <summary>
        /// 检查字段sql
        /// </summary>
        /// <remarks>
        /// 例如：
        /// </remarks>
        public static readonly string CheckFieldAction = "/App_Data/CheckFieldAction.txt";



        /// <summary>
        /// 素有系统的的AppDll
        /// </summary>
        /// <remarks>
        /// 例如：
        /// <ModelDlls name="售楼系统" code="SL" ycurl="">
        /// <Item>Mysoft.Slxt.Common.dll</Item>
        /// <Item>Mysoft.Slxt.ProjectPrep.dll</Item>
        /// <Item>Mysoft.Slxt.PriceMng.dll</Item>
        /// <Item>Mysoft.Slxt.TradeMng.dll</Item>
        /// <Item>Mysoft.Slxt.FinanceMng.dll</Item>
        /// <Item>Mysoft.Slxt.SaleService.dll</Item>
        /// <Item>Mysoft.Slxt.OpportunityMng.dll</Item>
        /// <Item>Mysoft.Slxt.Scyx.dll</Item> 
        /// </ModelDlls>
        /// <ModelDlls name="售楼系统" code="SL" ycurl="">
        /// <Item>Mysoft.Slxt.Common.dll</Item>
        /// <Item>Mysoft.Slxt.ProjectPrep.dll</Item>
        /// <Item>Mysoft.Slxt.PriceMng.dll</Item>
        /// <Item>Mysoft.Slxt.TradeMng.dll</Item>
        /// <Item>Mysoft.Slxt.FinanceMng.dll</Item>
        /// <Item>Mysoft.Slxt.SaleService.dll</Item>
        /// <Item>Mysoft.Slxt.OpportunityMng.dll</Item>
        /// <Item>Mysoft.Slxt.Scyx.dll</Item> 
        /// </ModelDlls> 
        /// </remarks>
        public static readonly string AllAppDataSouce = "/App_Data/AllAppDataSouce.xml";

        /// <summary>
        /// 云测平台接口地址
        /// </summary>
        public static readonly string YunCeUrl = ConfigurationManager.AppSettings["YunCeUrl"];


        /// <summary>
        /// 忽略多时区key
        /// </summary>
        public static readonly string DsqHl = ConfigurationManager.AppSettings["DsqHl"];

        /// <summary>
        /// 系统code,例如：售楼是0011
        /// </summary>
        public static readonly string SysCode = ConfigurationManager.AppSettings["SysCode"];

    }
}