﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    public class YmModel
    {
        /// <summary>
        /// 总数
        /// </summary>
        private int _totalCount = 0;
        /// <summary>
        /// 全开放
        /// </summary>
        private int _qkfCount = 0;
        /// <summary>
        /// 部分开放
        /// </summary>
        private int _bfCount = 0;
        /// <summary>
        /// 不开放
        /// </summary>
        private int _bkfCount = 0;
        /// <summary>
        /// ASPX页面
        /// </summary>
        private int _aspxCount = 0;

        /// <summary>
        /// 总数
        /// </summary>
        public int TotalCount
        {
            get { return _totalCount; }
            set { _totalCount = value; }
        }

        /// <summary>
        /// 全开放
        /// </summary>
        public int QkfCount
        {
            get { return _qkfCount; }
            set { _qkfCount = value; }
        }

        /// <summary>
        /// 部分开放
        /// </summary>
        public int BfCount
        {
            get { return _bfCount; }
            set { _bfCount = value; }
        }

        /// <summary>
        /// 不开放
        /// </summary>
        public int BkfCount
        {
            get { return _bkfCount; }
            set { _bkfCount = value; }
        }

        /// <summary>
        /// ASPX页面
        /// </summary>
        public int AspxCount
        {
            get { return _aspxCount; }
            set { _aspxCount = value; }
        }
        /// <summary>
        /// aspx不开放集合
        /// </summary>
        public List<string> ApsxBkfList
        {
            get
            {
                return _apsxBkfList;
            }

            set
            {
                _apsxBkfList = value;
            }
        }
        /// <summary>
        /// 普通页面不开放集合
        /// </summary>
        public List<string> PtBkfList
        {
            get
            {
                return _ptBkfList;
            }

            set
            {
                _ptBkfList = value;
            }
        }

        /// <summary>
        /// aspx不开放集合
        /// </summary>
        private List<String> _apsxBkfList = new List<string>();
        /// <summary>
        /// 普通页面不开放集合
        /// </summary>
        private List<String> _ptBkfList = new List<string>();
    }
}