﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace 检查页面开放.Models
{ 
    [XmlRoot(ElementName = "DataSouce")]
    public class FkTableModel
    {
        [XmlElement(ElementName = "table")]
        public List<Table> CommandList { get; set; }
    }

    public class Table
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }


        [XmlText]
        public string Sql { get; set; }
    }
}