﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Mysoft.ModelAnalysis.Models
{
    [XmlRoot("MetadataEntity")]
    public class MetadataEntity
    {
        /// <summary>
        /// 表名称
        /// </summary>
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [XmlAttribute(AttributeName = "DisplayName")]
        public string DisplayName { get; set; }
        /// <summary>
        /// 所属系统
        /// </summary>
        [XmlAttribute(AttributeName = "Application")]
        public string Application { get; set; }
        /// <summary>
        /// 属性列表
        /// </summary>
        [XmlElement("Attributes")]
        public Attributes Attributes{ get; set; }

        /// <summary>
        /// 是否启用公司分库
        /// </summary>
        [XmlAttribute(AttributeName = "IsEnableIsolateForCompany")] 
        public bool IsEnableIsolateForCompany { get; set; }

        /// <summary>
        /// 路径地址
        /// </summary>
        [XmlIgnore]
        public string FullName { get; set; }
    }

    public class Attributes
    {
        /// <summary>
        /// 属性列表
        /// </summary>
        [XmlElement("MetadataAttribute")]
        public List<MetadataAttribute> MetadataAttributes { get; set; }
    }
}