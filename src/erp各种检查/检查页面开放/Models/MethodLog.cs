﻿using EasyFramework.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    [TableAttribute("MethodLog")]
    public class MethodLog
    {
        [Column("Name")]
        public string Name { get; set; }

        [Column("Data")]
        public string Data { get; set; }

        [Column("CreateTime")]
        public DateTime CreateTime { get; set; }

        [Column("ID", ColumnType.IdentityAndPrimaryKey)]
        public int ID { get; set; }
    }
}