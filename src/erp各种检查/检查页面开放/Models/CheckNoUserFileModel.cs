﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    /// <summary>
    /// 检查无用文件
    /// </summary>
    public class CheckNoUserFileModel
    {
        private List<JsClass> _allJsName = new List<JsClass>();
        /// <summary>
        /// 所有js名称
        /// </summary>
        public List<JsClass> AllJsName { get { return _allJsName; } set { _allJsName = value; } }


        private List<AspxClass> _allAspxName = new List<AspxClass>();
        /// <summary>
        /// 所有aspx名称
        /// </summary>
        public List<AspxClass> AllAspxName { get { return _allAspxName; } set { _allAspxName = value; } }


        private List<string> _allNoUseFile = new List<string>();
        /// <summary>
        /// 无用文件
        /// </summary>
        public List<string> AllNoUseFile { get { return _allNoUseFile; } set { _allNoUseFile = value; } }
    }

    /// <summary>
    /// js对象
    /// </summary>
    public class JsClass
    {
        public  string Name { get; set; } 

        /// <summary>
        /// 匹配次数
        /// </summary>
        public int PpCount { get; set; }
    }

    /// <summary>
    /// Aspx对象
    /// </summary>
    public class AspxClass
    {
        public string Name { get; set; }

        /// <summary>
        /// 匹配次数
        /// </summary>
        public int PpCount { get; set; }
    }
}