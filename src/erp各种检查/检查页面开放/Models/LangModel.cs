﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace 检查页面开放.Models
{
    [XmlRoot(ElementName = "Langs")]
    public class LangModel
    {
        [XmlElement(ElementName = "List")]
        public List List { get; set; }

        [XmlElement("Language")]
        public string Language { get; set; }
    }

    public class List
    {
        [XmlElement(ElementName ="string")]
        public StringKey[] StringKeyList { get; set; } 
    }

    public class StringKey
    { 
        [XmlAttribute("Key")]
        public string Key { get; set; }


        [XmlText]
        public string Text { get; set; }
    }
}