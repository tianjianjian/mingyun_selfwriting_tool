﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    public class KeyValue
    {
        public  string Key { get; set; }

        public string Value { get; set; }

        /// <summary>
        /// 例如：.js
        /// </summary>
        public string FileExt { get; set; }
    }
}