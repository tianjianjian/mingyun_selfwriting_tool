﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    /// <summary>
    /// 分库结果
    /// </summary>
    public class FkResultDto
    {
        public string TableName { get; set; }

        public int Count { get; set; }

        public bool IsFk { get; set; }

        /// <summary>
        /// 是否计算了
        /// </summary>
        public bool IsCal { get; set; }
    }


    public class FkUIResult
    {
        /// <summary>
        /// --1、只要分库表没有数据（在分库上查询），过滤掉数据 > 0的数据 
        /// </summary>
        public List<FkResultDto> FkbNoData { get; set; }

        /// <summary>
        /// --2、只要分库的表（在主库上查询），显示数据 > 0的数据
        /// </summary>
        public List<FkResultDto> FkbZkNoData { get; set; }

        /// <summary>
        /// --3、不分库的表，在两个库上都有数据，数据要相等
        /// </summary>
        public List<FkResultDto> BfkbNoEqual { get; set; }

        /// <summary>
        /// 4、未计算的表
        /// </summary>
        public List<FkResultDto> NoCal { get; set; }
    }
}