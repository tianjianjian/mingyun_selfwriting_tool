﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ReadFileNames.Model
{
    [XmlRoot(ElementName = "grid")]
    public class GirdModel
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; } 
    }
}
