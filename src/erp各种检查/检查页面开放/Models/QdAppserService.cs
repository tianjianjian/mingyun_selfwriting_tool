﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    public class QdAppserService
    {
        private List<string> _dgAppserviceList = new List<string>();

        /// <summary>
        /// 多个Appservice的调用
        /// </summary>
        public List<string> DgAppserviceList
        {
            get
            {
                return _dgAppserviceList;
            }

            set
            {
                _dgAppserviceList = value;
            }
        }

        private List<string> _jsAjaxList = new List<string>();

        /// <summary>
        /// js中直接用ajax处理
        /// </summary>
        public List<string> JsAjaxList
        {
            get
            {
                return _jsAjaxList;
            }

            set
            {
                _jsAjaxList = value;
            }
        }

        /// <summary>
        /// js中不包含code       
        /// </summary>
        public List<string> NoContaintMcode
        {
            get { return _noContaintMcode; }
            set { _noContaintMcode = value; }
        }

        private List<string> _noContaintMcode = new List<string>();


        private List<string> _codeNoMatch = new List<string>();

        /// <summary>
        ///售楼路径
        /// </summary>
        public string SlPath { get; set; }

        /// <summary>
        /// js与AppService的code不一致
        /// </summary>
        public List<string> CodeNoMatch
        {
            get { return _codeNoMatch; }
            set { _codeNoMatch = value; }
        }
    }
}