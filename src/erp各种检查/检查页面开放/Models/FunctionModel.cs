﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ReadFileNames.Model
{
    [XmlRoot(ElementName = "functionPage")]
    public class FunctionModel
    {
        [XmlAttribute(AttributeName = "title")]
        public string Title { get; set; }

        [XmlAttribute(AttributeName = "pageName")]
        public string PageName { get; set; }
    }
}
