﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace 检查页面开放.Models
{
    [Serializable]
    public class YunCeModel
    {
        [JsonProperty("isSuccess")]
        public bool IsSuccess { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("sub_ticket")]
        public string Sub_ticket { get; set; }


        [JsonProperty("result")]
        public List<YcInterfaceModel> Result { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// "id":383,
    ///"user_id":49,
    ///"task_id":1,
    ///"tree_id":1931,
    ///"case_type":null,
    ///"case_name":"QueryRooms_查询房间",
    ///"case_desc":"根据查询条件返回房间信息",
    ///"script_path":"liqf",
    ///"create_time":"2019-05-07 18:06:28",
    ///"create_user":"李乾芳",
    ///"modify_time":"2019-08-14 09:37:38",
    ///"type":0,
    ///"origin_id":0,
    ///"data_template":null,
    ///"sequence":18,
    ///"project_id":595,
    ///"is_locked":0,
    ///"serial_sign":"",
    ///"status":1,
    ///"modify_user_id":49,
    ///"modify_user":"李乾芳",
    ///"origin_project_id":501,
    ///"origin_data_id":383,
    ///"is_enabled":1,
    ///"merge_sign":"",
    ///"route":"/pub/Mysoft.Slxt.ProjectPrep.Interfaces.IRoomGeneratePublicService/QueryRooms",
    ///"request_method":"POST",
    ///"module_name":"外部接口/房源管理"
    /// </remarks>
    [Serializable]
    public class YcInterfaceModel
    {
        [JsonProperty("route")]
        public string Route { get; set; }

        [JsonProperty("is_enabled")]
        public string IsEnabled { get; set; }
    }
}