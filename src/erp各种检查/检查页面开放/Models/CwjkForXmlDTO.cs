﻿using System;
using System.Xml;
using System.Xml.Serialization; 

namespace Mysoft.Slxt.FinanceMng.Model.DTO
{
    /// <summary>
    /// 财务账套读取xml模板解析类
    /// </summary>
    [XmlRoot(ElementName = "Cwzt")] 
    public class CwjkForXmlDTO  
    {
        /// <summary>
        /// 财务账套id
        /// </summary>
        [XmlAttribute(AttributeName = "CwztGUID")]
        public Guid CwztGUID { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        [XmlAttribute(AttributeName = "BuGUID")]
        public Guid BuGUID { get; set; }
        /// <summary>
        /// 是否是pos机
        /// </summary>
        [XmlAttribute(AttributeName = "PosModeEnum")]
        public int PosModeEnum { get; set; }
        /// <summary>
        /// 账套说明
        /// </summary>
        [XmlAttribute(AttributeName = "Remarks")]
        public string Remarks { get; set; }

        /// <summary>
        /// 辅助核算
        /// </summary>
        [XmlElement(ElementName = "Hsxms")]
        public Hsxms Hsxms
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目
        /// </summary>
        [XmlElement(ElementName = "Kjkms")]
        public Kjkms Kjkms
        {
            get;
            set;
        }
        /// <summary>
        /// 业务类型
        /// </summary>
        [XmlElement(ElementName = "OperTypes")]
        public OperTypes OperTypes
        {
            get;
            set;
        }

    }

    /// <summary>
    /// 单据财务账套读取xml模板解析类
    /// </summary>
    [XmlRoot(ElementName = "Cwzt")] 
    public class CwjkForVouchXmlDTO 
    {
        /// <summary>
        /// 财务账套id
        /// </summary>
        [XmlAttribute(AttributeName = "CwztGUID")]
        public Guid CwztGUID { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        [XmlAttribute(AttributeName = "BuGUID")]
        public Guid BuGUID { get; set; }
        /// <summary>
        /// 是否是pos机
        /// </summary>
        [XmlAttribute(AttributeName = "PosModeEnum")]
        public int PosModeEnum { get; set; }

        /// <summary>
        /// 接口类型
        /// </summary>
        [XmlAttribute(AttributeName = "InterfaceTypeEnum")]
        public int InterfaceTypeEnum { get; set; }

        /// <summary>
        /// 账套说明
        /// </summary>
        [XmlAttribute(AttributeName = "Remarks")]
        public string Remarks { get; set; }

        /// <summary>
        /// 辅助核算
        /// </summary>
        [XmlElement(ElementName = "Hsxms")]
        public Hsxms Hsxms
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目
        /// </summary>
        [XmlElement(ElementName = "Kjkms")]
        public Kjkms Kjkms
        {
            get;
            set;
        }
        /// <summary>
        /// 业务类型
        /// </summary>
        [XmlElement(ElementName = "VouchTypes")]
        public XmlElement VouchTypes
        {
            get;
            set;
        }

        ///// <summary>
        ///// 业务类型
        ///// </summary>
        //[XmlArray("VouchTypes")] 
        //public XmlElement[] VouchTypes
        //{
        //    get;
        //    set;
        //}
    }

    /// <summary>
    /// 单据类型数据
    /// </summary>
    public class VouchTypeC
    {
        /// <summary>
        /// 单据类型
        /// </summary>
        [XmlAttribute(AttributeName = "VouchTypes")]
        public string VouchTypes
        {
            get;
            set;
        }
        /// <summary>
        /// 是否禁用
        /// </summary>
        [XmlAttribute(AttributeName = "IsDisable")]
        public bool IsDisable
        {
            get;
            set;
        }
        /// <summary>
        /// 排序号
        /// </summary>
        [XmlAttribute(AttributeName = "Sequence")]
        public int Sequence
        {
            get;
            set;
        }

    }

    /// <summary>
    /// 辅助核算
    /// </summary>
    public class Hsxms
    {
        /// <summary>
        /// 辅助核算
        /// </summary>
        [XmlElement(ElementName = "Hsxm")]
        public Hsxm[] Hsxm
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 辅助核算
    /// </summary>
    public class Hsxm
    {
        /// <summary>
        /// 辅助核算主键
        /// </summary>
        [XmlAttribute(AttributeName = "HsTypeGUID")]
        public Guid HsTypeGuid
        {
            get;
            set;
        }

        /// <summary>
        /// 真实的guid
        /// </summary>
        public Guid HsTypeRealGuid { get; set; }

        /// <summary>
        /// 财务账套id
        /// </summary>
        [XmlAttribute(AttributeName = "CwztGUID")]
        public Guid CwztGuid
        {
            get;
            set;
        }
        /// <summary>
        /// 辅助核算名称
        /// </summary>
        [XmlAttribute(AttributeName = "HsTypeName")]
        public string HsTypeName
        {
            get;
            set;
        }
        /// <summary>
        /// 业务对象名称
        /// </summary>
        [XmlAttribute(AttributeName = "OperObject")]
        public string OperObject
        {
            get;
            set;
        }

        /// <summary>
        /// 业务对象code
        /// </summary>
        [XmlAttribute(AttributeName = "OperObjectCode")]
        public string OperObjectCode
        {
            get;
            set;
        }

        /// <summary>
        /// U8辅助核算名称
        /// </summary>
        [XmlAttribute(AttributeName = "HsTypeNameU8")]
        public string HsTypeNameU8
        {
            get;
            set;
        }
        /// <summary>
        /// 是否现金流量
        /// </summary>
        [XmlAttribute(AttributeName = "Flagitem")]
        public string FlagItem
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 会计科目
    /// </summary>
    public class Kjkms
    {
        /// <summary>
        /// 会计科目
        /// </summary>
        [XmlElement(ElementName = "Kjkm")]
        public Kjkm[] Kjkm
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 会计科目
    /// </summary>
    public class Kjkm
    {
        /// <summary>
        ///会计科目id
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmGUID")]
        public Guid KjkmGuid
        {
            get;
            set;
        }
        /// <summary>
        /// 父级会计科目id
        /// </summary>
        [XmlAttribute(AttributeName = "ParentGUID")]
        public Guid ParentGUID
        {
            get;
            set;
        }
        /// <summary>
        /// 会计客户真实的id
        /// 
        /// </summary>
        public Guid KjkmRealGuid
        {
            get;
            set;
        }
        /// <summary>
        /// 科目code
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmCode")]
        public string KjkmCode
        {
            get;
            set;
        }
        /// <summary>
        /// 科目名称
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmName")]
        public string KjkmName
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目级别
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmLevel")]
        public string KjkmLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 本级编码
        /// </summary>
        [XmlAttribute(AttributeName = "ShortCode")]
        public string ShortCode
        {
            get;
            set;
        }
        /// <summary>
        /// 父级编码
        /// </summary>
        [XmlAttribute(AttributeName = "ParentCode")]
        public string ParentCode
        {
            get;
            set;
        }
        /// <summary>
        /// 层级编码
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmOrderCode")]
        public string KjkmOrderCode
        {
            get;
            set;
        }
        /// <summary>
        /// 系统业务数据
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectDataText")]
        public string ObjectDataText
        {
            get;
            set;
        }
        /// <summary>
        /// 系统业务数据类型
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectType")]
        public string ObjectType
        {
            get;
            set;
        }
        /// <summary>
        /// 辅助核算
        /// </summary>
        [XmlAttribute(AttributeName = "HsTypeName")]
        public string HsTypeName
        {
            get;
            set;
        }
        /// <summary>
        /// 是否末级科目
        /// </summary>
        [XmlAttribute(AttributeName = "IsEnd")]
        public int IsEnd
        {
            get;
            set;
        }
        /// <summary>
        /// 是否系统级科目
        /// </summary>
        [XmlAttribute(AttributeName = "IsSys")]
        public int IsSys
        {
            get;
            set;
        }
        /// <summary>
        /// 所属业务对象值（二级科目才会有）
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectData")]
        public string ObjectData
        {
            get;
            set;
        }

        /// <summary>
        /// 所属业务对象
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectDataValues")]
        public string ObjectDataValues
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目父级名称
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmParentName")]
        public string KjkmParentName
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目全名
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmFullName")]
        public string KjkmFullName
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目2辅助核算表集合
        /// </summary>
        [XmlElement(ElementName = "Kjkm2Hsxms")]
        public Kjkm2Hsxms Kjkm2Hsxms
        {
            get;
            set;
        }

        /// <summary>
        /// 会计科目2业务数据
        /// </summary>
        [XmlElement(ElementName = "Kjkm2OperDatas")]
        public Kjkm2OperDatas Kjkm2OperDatas
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 会计科目2辅助核算表集合
    /// </summary>
    public class Kjkm2Hsxms
    {
        /// <summary>
        /// 会计科目2辅助核算表集合
        /// </summary>
        [XmlElement(ElementName = "Kjkm2Hsxm")]
        public Kjkm2Hsxm[] Kjkm2Hsxm { get; set; }
    }

    /// <summary>
    /// 会计科目2辅助核算表
    /// </summary>
    public class Kjkm2Hsxm
    {
        /// <summary>
        /// 会计科目id
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmGUID")]
        public Guid KjkmGuid
        {
            get;
            set;
        }
        /// <summary>
        /// 辅助核算id
        /// </summary>
        [XmlAttribute(AttributeName = "HsTypeGUID")]
        public Guid HsTypeGuid
        {
            get;
            set;
        }
        /// <summary>
        /// 核算序号
        /// </summary>
        [XmlAttribute(AttributeName = "HsNum")]
        public int HsNum
        {
            get;
            set;
        }

        /// <summary>
        /// 账套id
        /// </summary>
        [XmlAttribute(AttributeName = "CwztGUID")]
        public Guid CwztGUID
        {
            get;
            set;
        }

    }

    /// <summary>
    /// 会计科目2业务数据
    /// </summary>
    public class Kjkm2OperDatas
    {
        /// <summary>
        /// 会计科目2业务数据集合DTO
        /// </summary>
        [XmlElement(ElementName = "Kjkm2OperData")]
        public Kjkm2OperData[] Kjkm2OperData { get; set; }
    }

    /// <summary>
    /// 会计科目2业务数据
    /// </summary>
    public class Kjkm2OperData
    {
        /// <summary>
        /// 主键
        /// </summary>
        [XmlAttribute(AttributeName = "Kjkm2OperDataGUID")]
        public Guid Kjkm2OperDataGUID { get; set; }

        /// <summary>
        /// 账套id
        /// </summary>
        [XmlAttribute(AttributeName = "CwztGUID")]
        public Guid CwztGUID { get; set; }

        /// <summary>
        /// 公司id
        /// </summary>
        [XmlAttribute(AttributeName = "BUGUID")]
        public Guid BUGUID { get; set; }

        /// <summary>
        /// 款及科目id
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmGUID")]
        public Guid KjkmGUID { get; set; }

        /// <summary>
        /// 业务对象名称(如:一级产品类型+票据类型
        /// </summary>
        [XmlAttribute(AttributeName = "OperObject")]
        public string OperObject { get; set; }

        /// <summary>
        /// 业务对象guid
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectGUID")]
        public Guid ObjectGUID { get; set; }

        /// <summary>
        /// 业务数据名称(如:住宅+发票)
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectName")]
        public string ObjectName { get; set; }

        /// <summary>
        /// 一级会计科目Guid
        /// </summary>
        [XmlAttribute(AttributeName = "ObjectKjkmGUID")]
        public Guid ObjectKjkmGUID { get; set; }
    }

    /// <summary>
    /// 业务类型数据集合
    /// </summary>
    public class OperTypes
    {
        /// <summary>
        /// 业务类型数据集合
        /// </summary>
        [XmlElement(ElementName = "OperType")]
        public OperTypeC[] OperType
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 业务类型数据
    /// </summary>
    public class OperTypeC
    {
        /// <summary>
        /// 业务类型名称
        /// </summary>
        [XmlAttribute(AttributeName = "OperType")]
        public string OperType
        {
            get;
            set;
        }
        /// <summary>
        /// 业务类型级别
        /// </summary>
        [XmlAttribute(AttributeName = "OperTypeLevel")]
        public int OperTypeLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 票据类型
        /// </summary>
        [XmlAttribute(AttributeName = "InvoTypes")]
        public string InvoTypes
        {
            get;
            set;
        }
        /// <summary>
        /// 转出票据类型
        /// </summary>
        [XmlAttribute(AttributeName = "ZcInvoTypes")]
        public string ZcInvoTypes
        {
            get;
            set;
        }

        /// <summary>
        /// 数据源
        /// </summary>
        [XmlAttribute(AttributeName = "DataSource")]
        public string DataSource
        {
            get;
            set;
        }
        /// <summary>
        /// 过滤条件
        /// </summary>
        [XmlAttribute(AttributeName = "DataFilter")]
        public string DataFilter
        {
            get;
            set;
        }
        /// <summary>
        /// 二级过滤条件
        /// </summary>
        [XmlAttribute(AttributeName = "SecondFilter")]
        public string SecondFilter
        {
            get;
            set;
        }
        /// <summary>
        /// 摘要引用字段
        /// </summary>
        [XmlAttribute(AttributeName = "ZyFiled")]
        public string ZyFiled
        {
            get;
            set;
        }
        /// <summary>
        /// 金额引用字段
        /// </summary>
        [XmlAttribute(AttributeName = "AmountFiled")]
        public string AmountFiled
        {
            get;
            set;
        }
        /// <summary>
        /// 特殊凭证生成算法
        /// </summary>
        [XmlAttribute(AttributeName = "SpecialRule")]
        public string SpecialRule
        {
            get;
            set;
        }
        /// <summary>
        /// 本级代码
        /// </summary>
        [XmlAttribute(AttributeName = "OperTypeCode")]
        public string OperTypeCode
        {
            get;
            set;
        }
        /// <summary>
        /// 排序层级代码
        /// </summary>
        [XmlAttribute(AttributeName = "HierarchyCode")]
        public string HierarchyCode
        {
            get;
            set;
        }
        /// <summary>
        /// 排序编号
        /// </summary>
        [XmlAttribute(AttributeName = "Sequence")]
        public int Sequence { get; set; }

        /// <summary>
        /// 款项名称
        /// </summary>
        [XmlAttribute(AttributeName = "ItemNames")]
        public string ItemNames
        {
            get;
            set;
        }
        /// <summary>
        /// 转出款项名称
        /// </summary>
        [XmlAttribute(AttributeName = "ZcItemNames")]
        public string ZcItemNames
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute(AttributeName = "IsShowUsedScope")]
        public bool IsShowUsedScope
        {
            get;
            set;
        }
        /// <summary>
        /// 是否显示适用范围
        /// </summary>
        [XmlAttribute(AttributeName = "IsUsedScopeRepeat")]
        public bool IsUsedScopeRepeat
        {
            get;
            set;
        }
        /// <summary>
        /// 是否系统默认业务场景
        /// </summary>
        [XmlAttribute(AttributeName = "IsSys")]
        public bool IsSys
        {
            get;
            set;
        }
        /// <summary>
        /// 是否禁用
        /// </summary>
        [XmlAttribute(AttributeName = "IsDisable")]
        public bool IsDisable
        {
            get;
            set;
        }
        /// <summary>
        /// 是否可以新增业务场景
        /// </summary>
        [XmlAttribute(AttributeName = "IsAddible")]
        public bool IsAddible
        {
            get;
            set;
        }
        /// <summary>
        /// 凭证分录规则集合
        /// </summary>
        [XmlElement(ElementName = "EntryRules")]
        public EntryRules EntryRules
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 凭证分录规则
    /// </summary>
    public class EntryRules
    {
        /// <summary>
        /// 凭证分录规则集合
        /// </summary>
        [XmlElement(ElementName = "EntryRule")]
        public EntryRule[] EntryRule
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 凭证分录规则
    /// </summary>
    public class EntryRule
    {
        /// <summary>
        /// 摘要
        /// </summary>
        [XmlAttribute(AttributeName = "Zy")]
        public string Zy
        {
            get;
            set;
        }
        /// <summary>
        /// 摘要Sql
        /// </summary>
        [XmlAttribute(AttributeName = "ZySql")]
        public string ZySql
        {
            get;
            set;
        }
        /// <summary>
        /// 金额字段（英文名称）
        /// </summary>
        [XmlAttribute(AttributeName = "AmountFiled")]
        public string AmountFiled
        {
            get;
            set;
        }
        /// <summary>
        /// 借贷规则
        /// </summary>
        [XmlAttribute(AttributeName = "YDDirection")]
        public string YdDirection
        {
            get;
            set;
        }
        /// <summary>
        /// 原始借贷规则
        /// </summary>
        [XmlAttribute(AttributeName = "YsYDDirection")]
        public string YsYDDirection
        {
            get;
            set;
        }
        /// <summary>
        /// 科目SQL条件规则(SQL过滤条件)
        /// </summary>
        [XmlAttribute(AttributeName = "SqlFilterRule")]
        public string SqlFilterRule
        {
            get;
            set;
        }
        /// <summary>
        /// 金额规则
        /// </summary>
        [XmlAttribute(AttributeName = "IsInversion")]
        public int IsInversion
        {
            get;
            set;
        }
        /// <summary>
        /// 分录号
        /// </summary>
        [XmlAttribute(AttributeName = "EntryNum")]
        public int EntryNum
        {
            get;
            set;
        }
        /// <summary>
        /// 摘要Text
        /// </summary>
        [XmlAttribute(AttributeName = "ZyText")]
        public string ZyText
        {
            get;
            set;
        }
        /// <summary>
        /// 金额字段（中文名称）
        /// </summary>
        [XmlAttribute(AttributeName = "AmountFiledCN")]
        public string AmountFiledCn
        {
            get;
            set;
        }
        /// <summary>
        /// 会计科目GUID
        /// </summary>
        [XmlAttribute(AttributeName = "KjkmGUID")]
        public Guid KjkmGuid
        {
            get;
            set;
        }
        /// <summary>
        /// 凭证字
        /// </summary>
        [XmlAttribute(AttributeName = "Pzz")]
        public string Pzz
        {
            get;
            set;
        }
        /// <summary>
        /// 凭证名称
        /// </summary>
        [XmlAttribute(AttributeName = "PzName")]
        public string PzName
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute(AttributeName = "GroupName")]
        public string GroupName
        {
            get;
            set;
        }
        /// <summary>
        /// 分组名称
        /// </summary>
        [XmlAttribute(AttributeName = "IsDisable")]
        public bool IsDisable
        {
            get;
            set;
        }
        /// <summary>
        /// 凭证排序编码
        /// </summary>
        [XmlAttribute(AttributeName = "PzOrderCode")]
        public string PzOrderCode
        {
            get;
            set;
        }
        /// <summary>
        /// 是否允许新增分录
        /// </summary>
        [XmlAttribute(AttributeName = "IsAddEntry")]
        public int IsAddEntry
        {
            get;
            set;
        }
    }

}
