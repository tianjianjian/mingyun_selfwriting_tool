﻿using System.Web;
using System.Web.Mvc;

namespace 检查页面开放
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
