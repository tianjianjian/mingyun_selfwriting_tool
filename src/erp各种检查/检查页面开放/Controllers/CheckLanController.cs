﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 检查页面开放.Business;
using 检查页面开放.Models;
using 检查页面开放.Utility;

namespace 检查页面开放.Controllers
{
    /// <summary>
    /// 检查多语言重复key
    /// </summary>
    public class CheckLanController : Controller
    {
        // GET: CheckLan
        public ActionResult Index()
        {
            string path = Path.Combine(PathConst.YsjAddress, "Langs");
            CheckLanRepeat checkLanRepeat = new CheckLanRepeat(path);
            List<ErrorLang> errorLangList = checkLanRepeat.Check();
            return View(errorLangList);
        }
    }
}