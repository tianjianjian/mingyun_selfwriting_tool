﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ReadFileNames.Model;
using 检查页面开放.Business;
using 检查页面开放.Models;
using 检查页面开放.Utility;

namespace 检查页面开放.Controllers
{
    public class HomeController : Controller
    {


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult IndexList()
        {
            return View();
        }

        #region 开放页面检查
        public ActionResult ViewKf()
        {
            var ymModel = new ChekOpenList().ReadFile();
            return View(ymModel);
        }

        #endregion

        #region 检查 appservice是否夸模块调用
        /// <summary>
        /// Appservice调用检查
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewQdAppService()
        {
            QdAppserService qdAppserService = new QdAppserService();
            qdAppserService.SlPath = PathConst.SlAddress;
            ReadQdAppServiceFile(PathConst.SlJsPath, qdAppserService);
            return View(qdAppserService);
        }

        private void ReadQdAppServiceFile(string path, QdAppserService qdAppserService)
        {
            System.IO.DirectoryInfo dic = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Extension != ".js" || fileInfo.Name.EndsWith("LangRes.js") || fileInfo.Name.EndsWith("AppService.js"))
                    continue;
                string str2 = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);

                Regex reg = new Regex("(.).*(AppService)");
                MatchCollection matches = reg.Matches(str2);
                //是否有多个AppService
                if (matches.Count > 2)
                {
                    qdAppserService.DgAppserviceList.Add(fileInfo.FullName);
                }
                else
                {
                    //js中的code与Appservice的code不一致
                    CheckDefine(qdAppserService, str2, fileInfo);
                }
                //js中直接用ajax处理
                if (str2.IndexOf(".ajax(") > 0)
                {
                    qdAppserService.JsAjaxList.Add(fileInfo.FullName);
                }
            }
            foreach (DirectoryInfo cDic in dic.GetDirectories())
            {
                ReadQdAppServiceFile(cDic.FullName, qdAppserService);
            }
        }

        private void CheckDefine(QdAppserService qdAppserService, string str2, FileInfo fileInfo)
        {
            string fullName = fileInfo.FullName;
            Regex regCode = new Regex("(define).*(function)");
            MatchCollection cc = regCode.Matches(str2);
            if (cc.Count == 0)
            {
                return;
                //todo:throw new Exception(fullName + "js没有定义define");
            }
            string[] matchStrs = cc[0].ToString().Split(new char[] { '.' });

            //不是以M开头，需要检查
            if (matchStrs[2].StartsWith("M") == false)
            {
                qdAppserService.NoContaintMcode.Add(fullName);
            }
            else
            {
                string jsCode = matchStrs[2];
                //找Appservice
                Regex regAppservice = new Regex("(require)(.).*(AppService)");
                MatchCollection appserviceMatchs = regAppservice.Matches(str2);
                if (appserviceMatchs.Count != 0)
                {
                    string appName = appserviceMatchs[appserviceMatchs.Count - 1].ToString().Split(new char[] { '.' }).Last() + ".cs";

                    string[] expectAppservice = new string[] { "ProjectTreeAppService.cs", "projectTeamAppService.cs" };
                    if (expectAppservice.Contains(appName))
                        return;
                    string appServicePath = GetFileByName(qdAppserService.SlPath, appName);
                    if (string.IsNullOrEmpty(appServicePath))
                    {
                        throw new Exception(appName + "没找到");
                    }
                    string appServiceContent = System.IO.File.ReadAllText(appServicePath, Encoding.Default);
                    Regex appCode = new Regex(@"(AppServiceScope).*(\))");
                    string appCodeMatchs = appCode.Matches(appServiceContent)[0].ToString().Split(new char[] { ',' }).Last().Split(new char[] { ')' }).First();
                    //如果包含XX，不需要检查
                    if (appCodeMatchs.IndexOf("XX") != -1)
                    {
                        return;
                    }
                    if (appCodeMatchs.Trim() != ("\"" + jsCode.Remove(0, 1) + "\""))
                    {
                        qdAppserService.CodeNoMatch.Add(fullName);
                    }
                }
            }
        }

        /// <summary>
        /// 获取文件名称
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private string GetFileByName(string path, string name)
        {
            string mathName = "";
            System.IO.DirectoryInfo dic = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Name == name)
                {
                    mathName = fileInfo.FullName;
                    break;
                }
            }
            foreach (DirectoryInfo cDic in dic.GetDirectories())
            {
                if (string.IsNullOrEmpty(mathName) == false)
                {
                    break;
                }
                mathName = GetFileByName(cDic.FullName, name);
            }
            return mathName;
        }
        #endregion

        #region temp 获取表单上的内嵌网格设置了查询权限

        /// <summary>
        /// 获取表单上的内嵌网格设置了查询权限
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewGetFormNqWg()
        {
            List<string> nqGirdGuid = GetNqGrid("grid");
            nqGirdGuid.AddRange(GetNqGrid("treeGrid"));
            Dictionary<string, string> wentiGrid = Check(nqGirdGuid);
            return View(wentiGrid);
        }

        /// <summary>
        /// 获取表单内嵌网格
        /// </summary>
        private List<string> GetNqGrid(string gridType)
        {
            List<string> nqGirdGuid = new List<string>();
            DirectoryInfo di = new DirectoryInfo(PathConst.YsjAppFormAddress);
            foreach (FileInfo fileInfo in di.GetFiles())
            {
                if (fileInfo.Extension != ".config")
                    continue;
                string content = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);
                Regex rg = new Regex("(<" + gridType + ").*(/>)");
                MatchCollection matchs = rg.Matches(content);
                foreach (Match match in matchs)
                {
                    var str = match.ToString();
                    Regex rgGuid = new Regex("(metadataId=\").*(\")");
                    var guidStr = rgGuid.Match(str).ToString();
                    nqGirdGuid.Add(guidStr.Split(new char[] { '\"' })[1].Trim());
                }
            }
            return nqGirdGuid;
        }

        /// <summary>
        /// 遍历MyFunction，检查内嵌网格是否在里面设置了00权限
        /// </summary>
        private Dictionary<string, string> Check(List<string> nqGirdGuid)
        {
            Dictionary<string, string> wentiGrid = new Dictionary<string, string>();
            string formPath = @"E:\工作\TFS\ERP-V1.0\30_售楼系统\01_主干-开发分支\00_根目录\_metadata\MyFunction";
            string gridPath = @"E:\工作\TFS\ERP-V1.0\30_售楼系统\01_主干-开发分支\00_根目录\_metadata\AppGrid";
            DirectoryInfo di = new DirectoryInfo(formPath);
            foreach (FileInfo fileInfo in di.GetFiles())
            {
                if (fileInfo.Extension != ".config")
                    continue;
                string content = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);
                foreach (string girdGuid in nqGirdGuid)
                {
                    Regex rg = new Regex("(<right).*(controlId=\"" + girdGuid + "\").*(/>)");
                    MatchCollection matchs = rg.Matches(content);
                    if (matchs.Count > 0)
                    {
                        //name
                        string str2 = System.IO.File.ReadAllText(gridPath + "\\" + girdGuid + ".metadata.config".ToLower(), Encoding.ASCII);
                        GirdModel grid = XmlUtility.DeserializeToObject<GirdModel>(str2);
                        wentiGrid.Add(girdGuid, grid.Name);
                    }
                }
            }
            return wentiGrid;
        }
        #endregion

        #region 检查js、aspx是否还有用

        public ActionResult ViewCheckNoUserFile()
        {
            DateTime s = DateTime.Now;


            CheckNoUserFileModel checkNoUserFileModel = new CheckNoUserFileModel();
            CheckNoUserFile_GetFiles(PathConst.SlJsPath, checkNoUserFileModel);
            //检查元数据
            CheckNoUserFile_CheckNoUser(PathConst.YsjAddress, checkNoUserFileModel);
            //检查js
            CheckNoUserFile_CheckNoUser(PathConst.SlJsPath, checkNoUserFileModel);


            DateTime e = DateTime.Now;
            ViewBag.Start = s;
            ViewBag.End = e;
            return View(checkNoUserFileModel);
        }

        private void CheckNoUserFile_GetFiles(string path, CheckNoUserFileModel checkNoUserFileModel)
        {
            System.IO.DirectoryInfo dic = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Extension == ".js")
                {
                    checkNoUserFileModel.AllJsName.Add(new JsClass { Name = GetJsName(fileInfo) });
                }
                else if (fileInfo.Extension == ".aspx")
                {
                    checkNoUserFileModel.AllAspxName.Add(new AspxClass { Name = fileInfo.Name });
                }
            }
            foreach (DirectoryInfo cDic in dic.GetDirectories())
            {
                CheckNoUserFile_GetFiles(cDic.FullName, checkNoUserFileModel);
            }
        }

        private void CheckNoUserFile_CheckNoUser(string checkPath, CheckNoUserFileModel checkNoUserFileModel)
        {
            DirectoryInfo dic = new DirectoryInfo(checkPath);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Extension == ".js" || fileInfo.Extension == ".aspx" || fileInfo.FullName.EndsWith(".metadata.config",StringComparison.OrdinalIgnoreCase))
                {
                    string fileContent = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);
                    foreach (JsClass jsClass in checkNoUserFileModel.AllJsName)
                    {
                        if (jsClass.PpCount > 0)
                        {
                            continue;
                        }
                        if (fileInfo.Extension == ".js")
                        {
                            //如果js是自身，那么不用检查
                            Regex regCode = new Regex("(define).*(" + jsClass.Name + ").*(function)");
                            if (regCode.IsMatch(fileContent))
                            {
                                continue;
                            }
                        }

                        Regex regCode2 = new Regex(".*(" + jsClass.Name + ").*");
                        if (regCode2.Matches(fileContent).Count > 0)
                        {
                            jsClass.PpCount++;
                            continue;
                        }
                    }

                    if (fileInfo.FullName.EndsWith(".metadata.config",StringComparison.OrdinalIgnoreCase))
                    {
                        foreach (AspxClass aspxClass in checkNoUserFileModel.AllAspxName)
                        {
                            if (aspxClass.PpCount > 0)
                            {
                                continue;
                            }
                            Regex regCode2 = new Regex(".*(" + aspxClass.Name + ").*");
                            if (regCode2.IsMatch(fileContent))
                            {
                                aspxClass.PpCount++;
                                continue;
                            }
                        }
                    }
                }
            }
            foreach (DirectoryInfo cDic in dic.GetDirectories())
            {
                CheckNoUserFile_CheckNoUser(cDic.FullName, checkNoUserFileModel);
            }
        }

        private string GetJsName(FileInfo fileInfo)
        {
            Regex regCode = new Regex("(define).*(\"|\')");
            string fileContent = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);
            MatchCollection cc = regCode.Matches(fileContent);
            if (cc.Count == 0)
            {
                throw new Exception(fileInfo.FullName + "js没有定义define");
            }
            return cc[0].ToString().Split(new char[] { '\'', '"' })[1];
        }
        #endregion

        #region  检查无用的XMLCommand

        //文件 
        private Dictionary<string, string> csFileList = new Dictionary<string, string>();
        private List<XmlCommand> commandList = new List<XmlCommand>();
        private List<String> noUserXmlCommand = new List<string>();
        /// <summary>
        /// 检查无用的xmlcommand
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewCheckNoUserXMLCommand()
        {
            GetXmlCommadnFiles(PathConst.SlAddress);
            CheckNoUseXmlCommand();
            return View(noUserXmlCommand);
        }

        /// <summary>
        /// 获取xmlCommand文件
        /// </summary>
        /// <param name="path"></param>
        private void GetXmlCommadnFiles(string path)
        {
            DirectoryInfo diInfo = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in diInfo.GetFiles())
            {
                //如果是XmlCommand文件
                if (fileInfo.Name.EndsWith(".XmlCommand.Config"))
                {
                    ArrayOfXmlCommand ac = XmlUtility.DeserializeToObject<ArrayOfXmlCommand>(fileInfo.OpenText().ReadToEnd());
                    ////移除排除的项
                    //ac.CommandList.RemoveAll(t => expectCheck.Contains(t.Name) == true);
                    commandList.AddRange(ac.CommandList);
                    continue;
                }
                //是否是需要检查的项目
                if (fileInfo.Extension != ".cs")
                {
                    continue;
                }
                csFileList.Add(fileInfo.FullName, fileInfo.OpenText().ReadToEnd());
            }
            DirectoryInfo[] childrenDic = diInfo.GetDirectories();
            foreach (DirectoryInfo directoryInfo in childrenDic)
            {
                GetXmlCommadnFiles(directoryInfo.FullName);
            }
        }
        //检查无用的XmlCommand
        private void CheckNoUseXmlCommand()
        {
            foreach (var xmlCommand in commandList)
            {
                if (CheckXmlCommandKey(xmlCommand.Name) == false)
                {
                    noUserXmlCommand.Add(xmlCommand.Name);
                }
            }
        }
        //检查无用的key
        private bool CheckXmlCommandKey(string key)
        {
            bool isHas = false;
            foreach (var dic in csFileList)
            {
                string content = dic.Value;
                if (content.IndexOf(key) != -1)
                {
                    isHas = true;
                    break;
                }
            }

            return isHas;
        }
        #endregion

        #region 检查Appservice是否调用

        private Dictionary<string, string> _checkMethods = new Dictionary<string, string>();
        public ActionResult ViewCheckAppMethodIsUse()
        {
            //得到排除的类、方法
            List<string> exList = GetEx();
            //找出AppService.cs类、方法
            GetCheckMethod(PathConst.SlAddress, exList);
            //得到前端每个js内容
            GetAllJsContent(PathConst.SlJsPath);
            //检查js是否使用
            List<string> noUseList = CheckMethodIsUse(_checkMethods, _jsContent);
            return View(noUseList);
        }

        /// <summary>
        /// 得到排除的类、方法
        /// </summary>
        private List<string> GetEx()
        {
            List<string> exList = new List<string>();
            foreach (string str in System.IO.File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"App_Data\ExCheckAppMethod.txt"), Encoding.Default))
            {
                exList.Add(str.Trim());
            }
            foreach (string str in System.IO.File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"App_Data\ExCheckAppClass.txt"), Encoding.Default))
            {
                exList.Add(str.Trim());
            }
            return exList;
        }
        /// <summary>
        /// 找出AppService.cs类、方法
        /// </summary>
        /// <param name="url"></param>
        public void GetCheckMethod(string url, List<string> exList)
        {
            DirectoryInfo dic = new DirectoryInfo(url);

            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Name.IndexOf("AppService.cs", StringComparison.OrdinalIgnoreCase) != -1)
                {
                    GetMethod(fileInfo, exList);
                }
            }

            foreach (var directoryInfo in dic.GetDirectories())
            {
                GetCheckMethod(directoryInfo.FullName, exList);
            }
        }
        public void GetMethod(FileInfo fileInfo, List<string> exList)
        {
            string className = "";
            using (StreamReader sr2 = fileInfo.OpenText())
            {
                className = GetClassName(sr2.ReadToEnd());
                //如果类名称中有排除的方法，直接排除
                if (exList.Contains(className))
                {
                    return;
                }
            }
            //前一行
            string preStr = "";
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex(@"(public)[\s]{1,900}(virtual).{1,900}(\()");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    preStr = str;
                    continue;
                }
                while (true)
                {
                    var value = match.Groups[0].ToString();
                    if (string.IsNullOrEmpty(value) == true)
                    {
                        preStr = str;
                        break;
                    }
                    if (preStr.IndexOf("[ForbidHttp]", StringComparison.OrdinalIgnoreCase) != -1)
                    {
                        preStr = str;
                        break;
                    }
                    string method = value.Trim().Replace("(", "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Last();
                    //如果方法名称中有排除的方法，直接排除
                    if (exList.Contains(className + "." + method))
                    {
                        preStr = str;
                        break;
                    }
                    try
                    {
                        _checkMethods.Add(className + "." + method, method);
                    }
                    catch (Exception)
                    {

                        throw new Exception("键值重复：" + className + "." + method);
                    }
                    match = match.NextMatch();

                }
                preStr = str;
            }

        }
        private string GetClassName(string content)
        {
            Regex regex = new Regex(@"(class ).*(AppService).*(:)");
            Match match = regex.Match(content);
            //获取类名称
            string className = match.Groups[0].ToString().Replace("class", "").Replace(":", "").Trim();
            return className;
        }

        //js内容，key为js名称，
        private List<KeyValue> _jsContent = new List<KeyValue>();
        /// <summary>
        /// 得到前端每个js内容
        /// </summary>
        /// <param name="path"></param>
        private void GetAllJsContent(string path)
        {
            System.IO.DirectoryInfo dic = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if ((fileInfo.Extension == ".js" || fileInfo.Extension == ".aspx") == false || fileInfo.Name.EndsWith("LangRes.js") || fileInfo.Name.EndsWith("AppService.js"))
                    continue;
                string jsContent = System.IO.File.ReadAllText(fileInfo.FullName, Encoding.Default);
                try
                {
                    _jsContent.Add(new KeyValue { Key = fileInfo.Name, Value = jsContent, FileExt = fileInfo.Extension });
                }
                catch (Exception)
                {
                    throw new Exception("js重复：" + fileInfo.Name);
                }
            }
            foreach (DirectoryInfo cDic in dic.GetDirectories())
            {
                GetAllJsContent(cDic.FullName);
            }
        }

        /// <summary>
        /// 检查js是否使用
        /// </summary>
        public List<string> CheckMethodIsUse(Dictionary<string, string> checkMethods, List<KeyValue> jsContent)
        {
            List<string> noUseList = new List<string>();
            foreach (var checkMethod in checkMethods)
            {
                //前端没有使用
                if (CheckMethodImpl(jsContent, checkMethod.Value) == false)
                {
                    noUseList.Add(checkMethod.Key);
                }
            }
            return noUseList;
        }

        public bool CheckMethodImpl(List<KeyValue> jsConten, string checkKey)
        {
            foreach (var keyValuePair in jsConten)
            {
                string ckStr = checkKey;
                string content = keyValuePair.Value;
                if (keyValuePair.FileExt == ".js")
                {
                    ckStr = "Service." + checkKey;

                    if (content.IndexOf("setSubmitAction(\"" + checkKey + "\")", StringComparison.OrdinalIgnoreCase) != -1
                        || content.IndexOf("setSubmitAction('" + checkKey + "')", StringComparison.OrdinalIgnoreCase) != -1
                        )
                    {
                        return true;
                    }
                }
                else if (keyValuePair.FileExt == ".aspx")
                {
                    ckStr = "AppService/" + checkKey + ".aspx";
                }
                if (content.IndexOf(ckStr, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    return true;
                }

            }
            return false;
        }

        #endregion

        #region 检查私有方法

        private List<string> _privateMethods = new List<string>();
        public ActionResult ViewPrivateAppMethodIsUse()
        {
            //找出AppService.cs类、方法
            GePrivateMethod(PathConst.SlAddress);

            return View(_privateMethods);
        }
        /// <summary>
        /// 找出AppService.cs类、方法
        /// </summary>
        /// <param name="url"></param>
        public void GePrivateMethod(string url)
        {
            DirectoryInfo dic = new DirectoryInfo(url);

            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Extension == ".cs")
                {
                    GetPrivateMethod(fileInfo);
                }
            }

            foreach (var directoryInfo in dic.GetDirectories())
            {
                GePrivateMethod(directoryInfo.FullName);
            }
        }
        public void GetPrivateMethod(FileInfo fileInfo)
        {
            string className = "";
            using (StreamReader sr2 = fileInfo.OpenText())
            {
                className = GetClassName(sr2.ReadToEnd());
            }
            if (className.EndsWith("Test"))
            {
                return;
            }
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex(@"(private).{1,900}(\()");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    continue;
                }
                while (true)
                {
                    var value = match.Groups[0].ToString();
                    if (string.IsNullOrEmpty(value) == true)
                    {
                        break;
                    }
                    //string method = value.Trim().Replace("(", "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Last();
                    if (str.IndexOf("readonly", StringComparison.OrdinalIgnoreCase) > 0 ||
                        str.IndexOf("InitializeStub", StringComparison.OrdinalIgnoreCase) > 0)
                    {
                        break;
                    }

                    try
                    {
                        _privateMethods.Add($"类名称：{className}" + str);
                    }
                    catch (Exception)
                    {

                        throw new Exception("键值重复：" + str);
                    }
                    match = match.NextMatch();

                }
            }

        }


        #endregion

        #region 检查元数据中<comboBox 没有设置文本
        private List<string> _comboBoxNoSetText = new List<string>();
        public ActionResult ViewComboBoxNoSetText()
        {
            GeComboBoxNoSetText(PathConst.YsjAddress);

            return View(_comboBoxNoSetText);
        }
        /// <summary>
        /// 检查元数据中<comboBox 没有设置文本
        /// </summary>
        /// <param name="url"></param>
        public void GeComboBoxNoSetText(string url)
        {
            DirectoryInfo dic = new DirectoryInfo(url);

            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (string.Equals(fileInfo.Extension, ".config", StringComparison.OrdinalIgnoreCase))
                {
                    GetComboBoxNoSetText(fileInfo);
                }
            }

            foreach (var directoryInfo in dic.GetDirectories())
            {
                GeComboBoxNoSetText(directoryInfo.FullName);
            }
        }
        public void GetComboBoxNoSetText(FileInfo fileInfo)
        {
            foreach (string str in System.IO.File.ReadAllLines(fileInfo.FullName, Encoding.Default))
            {
                Regex regex = new Regex(@"(.*<comboBox).*");
                Match match = regex.Match(str);
                if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
                {
                    continue;
                }
                var value = match.Groups[0].ToString();
                if (string.IsNullOrEmpty(value) == true)
                {
                    break;
                }
                if (str.IndexOf("Enum\"", StringComparison.OrdinalIgnoreCase) > 0)
                {
                    continue;
                }
                Regex regex2 = new Regex("(redundancyField=\").*(\").*");
                Match match2 = regex2.Match(str);
                if (str.IndexOf("redundancyField", StringComparison.OrdinalIgnoreCase) <= 0 || string.IsNullOrEmpty(match2.Groups[0].ToString()) == true)
                {
                    _comboBoxNoSetText.Add($"文件：{fileInfo.Name}" + str); ;
                }
            }

        }
        #endregion

        #region 检查V3建模元数据和实体是否一致

        public ActionResult CheckV3Model()
        {
            CheckV3Model checkV3Model = new CheckV3Model();
            checkV3Model.Check();
            return View(checkV3Model.ResultList);
        }
        #endregion

        private List<string> mestaNames = new List<string>();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewFile()
        {
            GetFileName(PathConst.YsjAppFormAddress);
            return View(mestaNames);
        }

        private void GetFileName(string path)
        {
            DirectoryInfo dic = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                mestaNames.Add(fileInfo.Name);
            }
            foreach (DirectoryInfo directoryInfo in dic.GetDirectories())
            {
                GetFileName(directoryInfo.Name);
            }
        }



    }
}