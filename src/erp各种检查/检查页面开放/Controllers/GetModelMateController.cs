﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Mysoft.Utility;
using 检查页面开放.Business.Gnb;

namespace 检查页面开放.Controllers
{
    /// <summary>
    /// 获取包的元数据清单（例如：财务接口独立、电子签章）
    /// </summary>
    public class GetModelMateController : Controller
    {

        #region  获取元数据列表

        /// <summary>
        /// 获取元数据清单集合页面
        /// </summary>
        /// <returns></returns>
        public ActionResult GetMate()
        {
            return View();
        }

        /// <summary>
        /// 供前端调用
        /// </summary>
        /// <returns></returns>
        public string GetMateList(string gitPath, string feature)
        {
            return new GetGnbMeate().GetMateList(gitPath, feature);
        }

        #endregion

        #region 获取多语言信息

        /// <summary>
        /// 获取多语言信息页面
        /// </summary>
        /// <returns></returns>
        public string GetLangList(string gitPath, string feature,bool isBldm)
        {
            GetGnbLang getGnbLang=new GetGnbLang();
           return getGnbLang.GetLangList(gitPath,feature, isBldm).ToJson(); 
        }
       

        #endregion
    }


}