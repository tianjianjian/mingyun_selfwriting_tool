﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Mysoft.ModelAnalysis.Utility;
using 检查页面开放.Business;
using 检查页面开放.Models;

namespace 检查页面开放.Controllers
{
    public class GetRepeatMethodController : Controller
    {
        GetRepeatMethod repeatMethod = new GetRepeatMethod();
        // GET: GetRepeatMethod
        public ActionResult Index()
        {
            List<Models.MethodInfo> appList = repeatMethod.GetRepeat();
            return View(appList);
        }

        /// <summary>
        /// 获取非虚的方法
        /// </summary>
        /// <returns></returns>
        public ActionResult NoVirtualMethod()
        {
            List<Models.MethodInfo> methodList = new GetNoVirtualMethod().GetNoVitual();
            return View(methodList);
        }
    }

    public class GetRepeatMethod
    {
        public List<Models.MethodInfo> RepeatList { get; set; }

        public GetRepeatMethod()
        {
            this.RepeatList = new List<Models.MethodInfo>();
        }

        public List<Models.MethodInfo> GetRepeat()
        {
            List<AppDataSource> appDataSource = DataSourceHelp.GetModelDlls(HttpContext.Current.Server.MapPath(PathConst.AppDataSouce));
            foreach (var source in appDataSource)
            {
                Assembly ass = Assembly.LoadFrom(source.Path);
                List<Type> typeList = ass.GetTypes().ToList();
                typeList.RemoveAll(p => p.BaseType == null || (p.BaseType.Name == "AppService" || p.BaseType.Name == "DomainService" || p.BaseType.Name == "AggregateService") == false);

                foreach (var c in typeList)
                {
                    List<Models.MethodInfo> tempList = new List<Models.MethodInfo>();
                    var allMethod = c.GetMethods().Where(t => t.IsPrivate == false);
                    foreach (var m in allMethod)
                    {
                        if (m.DeclaringType.FullName != c.FullName)
                        {
                            continue;
                        }
                        tempList.Add(new Models.MethodInfo
                        {
                            ClassName = c.Name,
                            MethodName = m.Name
                        });
                    }

                    var tempCount = tempList.GroupBy(t => new { t.ClassName, t.MethodName }).Where(t => t.Count() > 1);
                    foreach (var app in tempCount)
                    {
                        RepeatList.Add(new Models.MethodInfo
                        {
                            ClassName = app.Key.ClassName,
                            MethodName = app.Key.MethodName
                        });
                    }
                }
            }

            return RepeatList;
        }
    }
    public class GetNoVirtualMethod
    {
        public List<Models.MethodInfo> NoVirtualtList { get; set; }

        public GetNoVirtualMethod()
        {
            this.NoVirtualtList = new List<Models.MethodInfo>();
        }

        List<string> ExpectList = new List<string> { "get_WorkingUnit", "GetType" };
        public List<Models.MethodInfo> GetNoVitual()
        {
            List<AppDataSource> appDataSource = DataSourceHelp.GetModelDlls(HttpContext.Current.Server.MapPath(PathConst.AppDataSouce));
            foreach (var source in appDataSource)
            {
                Assembly ass = Assembly.LoadFrom(source.Path);
                List<Type> typeList = ass.GetTypes().ToList();
                //  typeList.RemoveAll(p => (p.BaseType == null || (p.BaseType.Name == "AppService" || p.BaseType.Name == "DomainService" || p.BaseType.Name == "AggregateService") == false)
                //&& p.Name.EndsWith("AppService") == false && p.Name.EndsWith("DomainService") == false && p.Name.EndsWith("AggregateService") == false);
                typeList.RemoveAll(p => p.BaseType == null || (p.BaseType.Name == "AppService" || p.BaseType.Name == "DomainService" || p.BaseType.Name == "AggregateService") == false);

                //typeList = typeList.Where(t => t.Name == "BookingModiBackNumDomainService").ToList();
                foreach (var c in typeList)
                {
                    var n = c.FullName;
                    if (n.EndsWith("MySoftYkDomainService"))
                    {
                        var i = 1;
                    }
                    var allMethod = c.GetMethods().Where(t => t.IsPublic == true);
                    foreach (var m in allMethod)
                    {
                        if (m.Name == "CancelVouchers")
                        {
                            var k = 1;
                        }
                        if (m.Name.StartsWith("get_") || m.Name.StartsWith("set_") || m.IsStatic == true)
                        {
                            continue;
                        }
                        if (m.Name == "CheckAsyncExecutingTask")
                        {

                        }
                        if (ExpectList.Contains(m.Name))
                        {
                            continue;
                        }

                        if (m.IsVirtual == false || m.IsFinal)
                        {
                            NoVirtualtList.Add(new Models.MethodInfo
                            {
                                ClassName = c.FullName,
                                MethodName = m.Name
                            });
                        }

                    }

                }
            }

            return NoVirtualtList;
        }
    }
}