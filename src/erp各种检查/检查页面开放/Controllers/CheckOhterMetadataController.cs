﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 检查页面开放.Business;

namespace 检查页面开放.Controllers
{
    /// <summary>
    /// 检查是否已有其他元数据的文件
    /// </summary>
    public class CheckOhterMetadataController : Controller
    {
        // GET: CheckOhterMetadata
        public ActionResult Index()
        {

            CheckOhterMetadata checkOhterMetadata = new CheckOhterMetadata();

            List<string> notSlMeta = checkOhterMetadata.ReadFile();

            return View(notSlMeta);
        }
    }
}