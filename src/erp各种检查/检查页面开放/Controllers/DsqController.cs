﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 检查页面开放.Business.Dsq;
using 检查页面开放.Models;

namespace 检查页面开放.Controllers
{
    /// <summary>
    /// 多时区检查
    /// </summary>
    public class DsqController : Controller
    {
        // GET: Dsq
        public ActionResult Index()
        {
            CheckDsq checkDsq = new CheckDsq();
            checkDsq.Check(PathConst.SlAddress);
            return View(checkDsq.GetResult());
        }

        
    }
}