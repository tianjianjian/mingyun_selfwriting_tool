﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Mysoft.ModelAnalysis.Utility;
using Mysoft.Slxt.FinanceMng.Model.DTO;
using ReadFileNames.Model;
using 检查页面开放.Utility;

namespace 检查页面开放.Controllers
{
    public class Test2Controller : Controller
    {
        private string path = @"E:\工作\GIT\slxt2.0\src\00_根目录\App_Data\Slxt\FinanceMng\Template\单据模式.xml";
        private readonly string _oppPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\Opp.txt");
        private readonly string _updateOppPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\UpdateOpp.txt");
        private readonly string _updateOppGuid = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\UpdateOppGuid.xml");
        public ActionResult Test1()
        {
            string str2 = System.IO.File.ReadAllText(path, Encoding.ASCII);
            var c = XmlUtility.DeserializeToObject<CwjkForVouchXmlDTO>(str2);
            return View();
        }

        public ActionResult GetOppJson()
        {
            string oppJson = System.IO.File.ReadAllText(_oppPath, Encoding.ASCII);
            var num = 1000200;
            List<string> jsonList = new List<string>();
            for (int i = 1; i <= 100; i++)
            {
                var json = oppJson.Replace("{index}", i.ToString()).Replace("{tel}", (num++).ToString());
                jsonList.Add(json);
            }

            ViewBag.OppJson = "[" + string.Join(",", jsonList) + "]";

            List<string> oppGuidList = DataSourceHelp.GetItems(_updateOppGuid);

            string updateOppJson = System.IO.File.ReadAllText(_updateOppPath, Encoding.ASCII);
            List<string> updateJsonList = new List<string>();
            for (int i = 1; i <= 100; i++)
            {
                string[] oppGuid = oppGuidList[i - 1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                var json = updateOppJson.Replace("{index}", "update" + i.ToString()).Replace("{tel}", (num++).ToString())
                    .Replace("{OppGUID}", oppGuid[0]).Replace("{OppCstGUID}", oppGuid[1]);
                updateJsonList.Add(json);
            }

            ViewBag.UpdateOppJson = "{    \"updateOppParams\":[" + string.Join(",", updateJsonList) + "], \"allowTelRepeat\": true}";

            return View();
        }
    }
}
