﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using 检查页面开放.Models;

namespace Mysoft.ModelAnalysis.Utility
{
    public static class DataSourceHelp
    {
        public static string GetEntityMetadataPath()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/EntityDllSouce.xml"));

            return Path.Combine(PathConst.RootPath, @"_metadata\Entity\");
        }

        public static List<AppDataSource> GetModelDlls(string path)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(path);
            XmlNodeList nodeList = xml.SelectNodes("/DataSouce/ModelDlls/Item");
            List<AppDataSource> appDataSources = new List<AppDataSource>();
            foreach (XmlNode node in nodeList)
            {
                var name = node.Attributes["name"].Value.ToString();
                appDataSources.Add(new AppDataSource
                {
                    Name = name,
                    Path = Path.Combine(PathConst.RootPath, @"bin\" + node.InnerText)
                });
            }
            return appDataSources;
        }

        public static List<AllAppDataSource> GetAllModelDlls(string xmlPath)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(xmlPath);
            XmlNodeList modelDlls = xml.SelectNodes("/DataSouce/ModelDlls");
            List<AllAppDataSource> allAppDataSources = new List<AllAppDataSource>();
            foreach (XmlNode node in modelDlls)
            {
                AllAppDataSource allAppDataSource = new AllAppDataSource();
                allAppDataSource.Name = node.Attributes["name"].Value.ToString();
                allAppDataSource.Code = node.Attributes["code"].Value.ToString();
                allAppDataSource.Ycurl = node.Attributes["ycurl"].Value.ToString();
                //代码地址
                var dmAdrress = node.Attributes["path"].Value.ToString();
                
                var itemNodes = node.SelectNodes("Item");
                List<AppDataSource> appDataSources = new List<AppDataSource>();
                foreach (XmlNode itemNode in itemNodes)
                {
                    var name = itemNode.Attributes["name"].Value.ToString();
                    appDataSources.Add(new AppDataSource
                    {
                        Name = name,
                        Path = Path.Combine(dmAdrress, @"bin\" + itemNode.InnerText)
                    });
                }
                allAppDataSource.AppDataSources = appDataSources;
                allAppDataSources.Add(allAppDataSource); 
            }
            return allAppDataSources;
        }

        public static List<string> GetItems(string path)
        {
            List<string> rst = new List<string>();
            XmlDocument xml = new XmlDocument();
            xml.Load(path);
            XmlNodeList nodeList = xml.SelectNodes("/DataSouce/ModelDlls/Item");
            foreach (XmlNode node in nodeList)
            {
                rst.Add(node.InnerText);
            }
            return rst;
        }


        /// <summary>
        /// 获取txt文件中的行集合
        /// </summary>
        public static List<string> GetTxtList(string filePath)
        {
            List<string> exList = new List<string>();
            foreach (string str in System.IO.File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + filePath), Encoding.Default))
            {
                exList.Add(str.Trim());
            }
            return exList;
        }
    }
}