﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 检查无用后端方法工具
{
    public partial class Form1 : Form
    {
        private string codeUrl = "";

        private Dictionary<string, string> checkMethods = new Dictionary<string, string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            codeUrl = this.txtUrl.Text.Trim();
            if (string.IsNullOrEmpty(codeUrl))
            {
                MessageBox.Show("请输入代码路径！");
                return;
            }
            txtResult.Text = "";
            SetCheckMethod(codeUrl);
            CheckMethod();
        }

        public void SetCheckMethod(string url)
        {
            DirectoryInfo dic = new DirectoryInfo(url);

            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Name.IndexOf("AppService.cs") != -1)
                {
                    StreamReader sr = fileInfo.OpenText();
                    string content = sr.ReadToEnd();

                    GetMethod(content);
                }
            }

            foreach (var directoryInfo in dic.GetDirectories())
            {
                SetCheckMethod(directoryInfo.FullName);
            }
        }

        public void CheckMethod()
        {
            var qdUrl = codeUrl + "\\00_根目录\\Slxt";
            foreach (var checkMethod in checkMethods)
            {
                //前端没有使用
                if (CheckMethodImpl(qdUrl, checkMethod.Value) == false)
                {
                    txtResult.AppendText(checkMethod.Key+"："+checkMethod.Value + "\r\n");
                }
            }
        }

        public bool CheckMethodImpl(string url, string key)
        {
            DirectoryInfo dic = new DirectoryInfo(url);

            foreach (FileInfo fileInfo in dic.GetFiles())
            {
                if (fileInfo.Name.IndexOf("AppService.js") == -1)
                {
                    continue;
                }

                StreamReader sr = fileInfo.OpenText();
                string content = sr.ReadToEnd();
                if (content.IndexOf("." + key) != -1)
                {
                    return true;
                }
            }

            foreach (var directoryInfo in dic.GetDirectories())
            {
                bool re = CheckMethodImpl(directoryInfo.FullName, key);
                if (re == true)
                {
                    return true;
                }
            }
            return false;
        }

        public void GetMethod(string content)
        {
            string className = GetClassName(content);
            Regex regex = new Regex(@"(public)[\s]{1,900}(virtual).{1,900}(\()");
            Match match = regex.Match(content);
            if (string.IsNullOrEmpty(match.Groups[0].ToString()) == true)
            {
                txtResult.AppendText(className + "：类为空！\r\n");
            }

            while (true)
            {
                var value = match.Groups[0].ToString();
                if (string.IsNullOrEmpty(value) == true)
                {
                    break;
                }
                string method = value.Trim().Replace("(", "").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Last();
                checkMethods.Add(className + "." + method, method);
                match = match.NextMatch();
            }
        }

        private string GetClassName(string content)
        {
            Regex regex = new Regex(@"(class ).*(AppService).*(:)");
            Match match = regex.Match(content);
            //获取类名称
            string className = match.Groups[0].ToString().Replace("class", "").Replace(":", "").Trim();
            return className;
        }

        private string GetNameSpace(string content)
        {
            Regex regex = new Regex(@"(namespace ).*(.AppService)");
            Match match = regex.Match(content);
            //获取namespace
            string namespaceStr = match.Groups[0].ToString().Replace("namespace", "").Trim();
            return namespaceStr;
        }

    }
}
