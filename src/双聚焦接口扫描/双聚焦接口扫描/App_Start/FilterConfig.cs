﻿using System.Web;
using System.Web.Mvc;

namespace 双聚焦接口扫描
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
