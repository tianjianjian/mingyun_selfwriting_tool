﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Helpers;
using Newtonsoft.Json;
using 双聚焦接口扫描.Models;

namespace 双聚焦接口扫描.Utility
{
    public class SjjInterfaceHelper
    {
        private static readonly string InterfaceJsonUrl = System.Configuration.ConfigurationManager.AppSettings["InterfaceJsonUrl"];
        public string GetJson()
        {
            HttpWebRequest httpReq;
            HttpWebResponse httpResp;

            string strBuff = "";
            char[] cbuffer = new char[256];
            ;

            Uri httpURL = new Uri(InterfaceJsonUrl);

            //HttpWebRequest类继承于WebRequest，并没有自己的构造函数，需通过WebRequest的Creat方法 建立，并进行强制的类型转换 
            httpReq = (HttpWebRequest)WebRequest.Create(httpURL);
            //通过HttpWebRequest的GetResponse()方法建立HttpWebResponse,强制类型转换

            httpResp = (HttpWebResponse)httpReq.GetResponse();
            //GetResponseStream()方法获取HTTP响应的数据流,并尝试取得URL中所指定的网页内容

            //若成功取得网页的内容，则以System.IO.Stream形式返回，若失败则产生ProtoclViolationException错 误。在此正确的做法应将以下的代码放到一个try块中处理。这里简单处理 
            Stream respStream = httpResp.GetResponseStream();

            //返回的内容是Stream形式的，所以可以利用StreamReader类获取GetResponseStream的内容，并以

            //StreamReader类的Read方法依次读取网页源程序代码每一行的内容，直至行尾（读取的编码格式：UTF8） 
            StreamReader respStreamReader = new StreamReader(respStream, Encoding.UTF8);

            int byteRead = respStreamReader.Read(cbuffer, 0, 256);

            while (byteRead != 0)
            {
                string strResp = new string(cbuffer, 0, byteRead);
                strBuff = strBuff + strResp;
                byteRead = respStreamReader.Read(cbuffer, 0, 256);
            }

            respStream.Close();
            return strBuff;
        }

        private string ReadTxt()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\Sjj.json");
            return FileToString(path);
        }
        public string FileToString(String filePath)
        {
            StringBuilder strData = new StringBuilder();
            try
            {
                string line;
                // 创建一个 StreamReader 的实例来读取文件 ,using 语句也能关闭 StreamReader
                using (System.IO.StreamReader sr = new System.IO.StreamReader(filePath))
                {
                    // 从文件读取并显示行，直到文件的末尾 
                    while ((line = sr.ReadLine()) != null)
                    {
                        //Console.WriteLine(line);
                        strData.AppendLine(line);
                    }
                }
            }
            catch (Exception e)
            {
                // 向用户显示出错消息
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return strData.ToString();
        }

        public string GetSwagger()
        {
            String str = "";
            //try
            //{
            //    str = GetJson();
            //}
            //catch (Exception e)
            //{
            return ReadTxt();
            //}
            //Json. 
        }
    }
}