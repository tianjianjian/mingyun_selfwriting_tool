﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace 双聚焦接口扫描.Models
{
    public class SwaggerDTO
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("swagger")]
        public string Swagger { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("schemes")]
        public string[] Schemes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("paths")]
        public List<PathJson> Paths { get; set; }
    }

    public class PathJson
    {
        [JsonProperty("post")]
        public Post Post { get; set; }
    }

    public class Post
    {
        [JsonProperty("operationId")]
        public string OperationId { get; set; }
    }
}