﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using 双聚焦接口扫描.Utility;

namespace 双聚焦接口扫描.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult SjjInterface()
        {
            SjjInterfaceHelper sjjInterfaceHelper = new SjjInterfaceHelper();
            var swagger = sjjInterfaceHelper.GetSwagger();
            ViewBag.SwaggerJson = swagger;
            return View();
        }
    }
}